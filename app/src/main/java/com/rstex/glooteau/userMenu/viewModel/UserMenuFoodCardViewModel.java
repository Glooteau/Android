package com.rstex.glooteau.userMenu.viewModel;

import android.view.View;
import android.widget.ImageButton;

import com.rstex.glooteau.R;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.food.viewModel.FoodCardViewModel;
import com.rstex.glooteau.userMenu.view.UserMenuFoodCardViewHolder;
import com.rstex.glooteau.user.modelController.UserModelController;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 21/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class UserMenuFoodCardViewModel extends FoodCardViewModel implements UserMenuFoodCardViewHolder.MenuFoodCardViewModel {

    public UserMenuFoodCardViewModel(Food food) {
        super(food);
    }

    @Override
    public void setupButton1(final ImageButton button) {
        button.setImageResource(R.drawable.ic_clear_black_24dp);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserModelController.removeFromMenu(mFood);
            }
        });
    }
}
