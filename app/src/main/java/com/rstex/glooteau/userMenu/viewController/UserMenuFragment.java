package com.rstex.glooteau.userMenu.viewController;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.rstex.glooteau.R;
import com.rstex.glooteau.common.utils.IntentConstants;
import com.rstex.glooteau.common.viewController.DetailsActivity;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.food.modelController.FoodModelController;
import com.rstex.glooteau.food.viewController.FoodDetailsEditingFragment;
import com.rstex.glooteau.food.viewController.FoodDetailsFragment;
import com.rstex.glooteau.user.modelController.UserModelController;
import com.rstex.glooteau.userMenu.view.UserMenuFoodCardViewHolder;
import com.rstex.glooteau.userMenu.viewModel.UserMenuFoodCardViewModel;

import static com.rstex.glooteau.common.utils.IntentConstants.DETAIL_FRAGMENT_CLASS;
import static com.rstex.glooteau.common.utils.IntentConstants.KEY;
import static com.rstex.glooteau.common.utils.IntentConstants.SCOPE;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 30/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class UserMenuFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_menu_main, container, false);

        ImageButton createFoodButton = view.findViewById(R.id.createFoodImageButton);
        createFoodButton.setOnClickListener(view12 -> {
            getActivity().onBackPressed();
            Intent intent = new Intent(getContext(), DetailsActivity.class);
            intent.putExtra(DETAIL_FRAGMENT_CLASS, FoodDetailsEditingFragment.class);
            intent.putExtra(SCOPE, IntentConstants.Scope.Menu);
            startActivity(intent);
        });

        ImageButton clearMenuButton = view.findViewById(R.id.clearMenuImageButton);
        clearMenuButton.setOnClickListener(view13 -> UserModelController.clearMenu());

        DatabaseReference foodRef = FoodModelController.getFoodInfoReference();
        DatabaseReference menuIndexRef = UserModelController.getIndexReference(UserModelController.IndexScope.Menu);

        if (menuIndexRef == null) {
            return view;
        }

        FirebaseRecyclerOptions<Food> firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Food>()
                .setIndexedQuery(menuIndexRef, foodRef, Food.class)
                .setLifecycleOwner(this)
                .build();

        FirebaseRecyclerAdapter recyclerAdapter = new FirebaseRecyclerAdapter<Food, UserMenuFoodCardViewHolder>(firebaseRecyclerOptions) {
            @Override
            public UserMenuFoodCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new UserMenuFoodCardViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.card_view_food_user_menu, parent, false));
            }

            @Override
            public void onBindViewHolder(UserMenuFoodCardViewHolder holder, int position, final Food food) {
                food.setKey(getRef(position).getKey());
                holder.setViewModel(new UserMenuFoodCardViewModel(food));

                holder.itemView.setOnClickListener(view1 -> {
                    Intent intent = new Intent(getContext(), DetailsActivity.class);
                    intent.putExtra(DETAIL_FRAGMENT_CLASS, FoodDetailsFragment.class);
                    intent.putExtra(SCOPE, IntentConstants.Scope.Menu);
                    intent.putExtra(KEY, food.getKey());
                    startActivity(intent);
                });
            }
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);

        return view;
    }
}
