package com.rstex.glooteau.user.model;

import com.google.firebase.database.Exclude;
import com.rstex.glooteau.common.model.FirebaseObject;
import com.rstex.glooteau.business.model.Business;

import java.util.HashMap;
import java.util.Map;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 06/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class User extends FirebaseObject {
    //Info
    private String mName;
    private String mProfileImageRef;
    private int mFollowersCount;

    //Indexes
    private HashMap<String, Boolean> mMenu;
    private HashMap<String, Boolean> mBookmarks;
    private HashMap<String, Boolean> mFollowing;

    //Business
    private Business mBusiness;

    public User() {

    }

    public User(String key, String name, String profileImageRef, int followersCount, HashMap<String, Boolean> menu, HashMap<String, Boolean> bookmarks, HashMap<String, Boolean> following, Business business) {
        super(key);
        mName = name;
        mProfileImageRef = profileImageRef;
        mFollowersCount = followersCount;
        mMenu = menu;
        mBookmarks = bookmarks;
        mFollowing = following;
        mBusiness = business;
    }

    @Exclude
    public Map<String, Object> toInfoMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", mName);
        result.put("profileImageRef", mProfileImageRef);
        result.put("followersCount", mFollowersCount);
        return result;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getProfileImageRef() {
        return mProfileImageRef;
    }

    public void setProfileImageRef(String profileImageRef) {
        mProfileImageRef = profileImageRef;
    }

    public int getFollowersCount() {
        return mFollowersCount;
    }

    public void setFollowersCount(int followersCount) {
        mFollowersCount = followersCount;
    }

    @Exclude
    public HashMap<String, Boolean> getMenu() {
        return mMenu;
    }

    public void setMenu(HashMap<String, Boolean> menu) {
        mMenu = menu;
    }

    @Exclude
    public HashMap<String, Boolean> getBookmarks() {
        return mBookmarks;
    }

    public void setBookmarks(HashMap<String, Boolean> bookmarks) {
        mBookmarks = bookmarks;
    }

    @Exclude
    public HashMap<String, Boolean> getFollowing() {
        return mFollowing;
    }

    public void setFollowing(HashMap<String, Boolean> following) {
        mFollowing = following;
    }

    @Exclude
    public Business getBusiness() {
        return mBusiness;
    }

    public void setBusiness(Business business) {
        mBusiness = business;
    }
}
