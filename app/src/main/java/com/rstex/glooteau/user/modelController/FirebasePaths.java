package com.rstex.glooteau.user.modelController;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 17/10/17.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

interface FirebasePaths {
    //RealTimeDatabase
    String USER_PATH = "user";
    String USER_INFO_PATH = USER_PATH + "/info";
    String USER_DIET_PATH = USER_PATH + "/diet";
    String USER_MENU_PATH = USER_PATH + "/menu";
    String USER_BOOKMARKS_PATH = USER_PATH + "/bookmarks";
    String USER_FOLLOWING_PATH = USER_PATH + "/following";

    //Storage
    String USER_PROFILE_IMAGE_STORAGE_PATH = USER_PATH + "/profileImage";
}
