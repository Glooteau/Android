package com.rstex.glooteau.user.modelController;

import android.net.Uri;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.business.model.Business;
import com.rstex.glooteau.user.model.User;

import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;
import static com.rstex.glooteau.user.modelController.FirebasePaths.USER_BOOKMARKS_PATH;
import static com.rstex.glooteau.user.modelController.FirebasePaths.USER_FOLLOWING_PATH;
import static com.rstex.glooteau.user.modelController.FirebasePaths.USER_MENU_PATH;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 17/10/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */
public abstract class UserModelController {
    private static final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private static final DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();

    public enum IndexScope {
        Bookmarks,
        Following,
        Menu
    }

    private static FirebaseUser getAuthUser() {
        return mAuth.getCurrentUser();
    }

    public static String getUserId() {
        if (getAuthUser() != null) {
            return getAuthUser().getUid();
        }
        return null;
    }

    public static String getUserName() {
        if (getAuthUser() != null) {
            return getAuthUser().getDisplayName();
        }
        return null;
    }

    public static Uri getUserImage() {
        if (getAuthUser() != null) {
            return getAuthUser().getPhotoUrl();
        }
        return null;
    }

    public static boolean isSignedIn() {
        return getAuthUser() != null && !getAuthUser().isAnonymous();
    }

    public static void ensureSignIn() {
        if (!isSignedIn()) {
            mAuth.signInAnonymously();
        }
    }

    public static void signUp() {

    }

    public static void logOut() {

    }

    public static void deleteAcount() {

    }

    //Common
    public static DatabaseReference getIndexReference(IndexScope scope) {
        String path, userId = nullableToString(getUserId());
        if (userId.isEmpty()) {
            return null;
        }

        switch (scope) {
            case Bookmarks:
                path = USER_BOOKMARKS_PATH;
                break;
            case Following:
                path = USER_FOLLOWING_PATH;
                break;
            case Menu:
                path = USER_MENU_PATH;
                break;
            default:
                throw new AssertionError("Invalid scope");
        }

        return mDatabaseReference.child(path).child(userId);
    }

    private static DatabaseReference getScopedReference(String scope, String childId) {
        String userId = nullableToString(getUserId());
        if (nullableToString(scope).isEmpty() || nullableToString(childId).isEmpty() || userId.isEmpty()) {
            return null;
        }
        return mDatabaseReference.child(scope).child(userId).child(childId);
    }

    //Info
    public static User getUser() {
        return null;
    }

    public static Business getBusiness() {
        return null;
    }

    public static Business getBusiness(String key) {
        return null;
    }

    //Bookmark
//    public static List<String> getBookmarkIndex() {
//        return null;
//    }

    public static void addBookmark(Food food) {
        if (food == null) {
            return;
        }
        DatabaseReference reference = getScopedReference(USER_BOOKMARKS_PATH, food.getKey());
        if (reference != null) {
            reference.setValue(true);
        }
    }

    public static void removeBookmark(Food food) {
        if (food == null) {
            return;
        }
        DatabaseReference reference = getScopedReference(USER_BOOKMARKS_PATH, food.getKey());
        if (reference != null) {
            reference.removeValue();
        }
    }

    //Following
//    public static void getFollowingList() {
//
//    }

    public static void addToFollowing(Business business) {
        if (business == null) {
            return;
        }
        DatabaseReference reference = getScopedReference(USER_FOLLOWING_PATH, business.getKey());
        if (reference != null) {
            reference.setValue(true);
        }
    }

    public static void removeFromFollowing(Business business) {
        if (business == null) {
            return;
        }
        DatabaseReference reference = getScopedReference(USER_FOLLOWING_PATH, business.getKey());
        if (reference != null) {
            reference.removeValue();
        }
    }

    //Menu
//    public static void getMenuList() {
//
//    }

    public static void addToMenu(Food food) {
        if (food == null) {
            return;
        }
        DatabaseReference reference = getScopedReference(USER_MENU_PATH, food.getKey());
        if (reference != null) {
            reference.setValue(true);
        }
    }

    public static void removeFromMenu(Food food) {
        if (food == null) {
            return;
        }
        DatabaseReference reference = getScopedReference(USER_MENU_PATH, food.getKey());
        if (reference != null) {
            reference.removeValue();
        }
    }

    public static void clearMenu() {
        DatabaseReference reference = getIndexReference(IndexScope.Menu);
        if (reference != null) {
            reference.removeValue();
        }
    }
}
