package com.rstex.glooteau.user.viewController;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.rstex.glooteau.R;
import com.rstex.glooteau.business.viewController.BusinessMenusDetailFragment;
import com.rstex.glooteau.common.viewController.AboutDialogFragment;
import com.rstex.glooteau.common.viewController.DetailsActivity;
import com.rstex.glooteau.user.modelController.UserModelController;

import java.util.Arrays;

import static android.app.Activity.RESULT_OK;
import static com.rstex.glooteau.common.utils.IntentConstants.DETAIL_FRAGMENT_CLASS;
import static com.rstex.glooteau.common.utils.LayoutUtils.lockAppBarLayoutScrolling;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 25/08/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class UserMainFragment extends Fragment {
    private static final int AUTH_CODE = 5374;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_main, container, false);

        final FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(v -> {
            if (UserModelController.isSignedIn()) {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                intent.putExtra(DETAIL_FRAGMENT_CLASS, UserEditingFragment.class);
                startActivity(intent);
            } else {
                startActivityForResult(AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(Arrays.asList(
                                new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                                new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()))
                        .build(), AUTH_CODE);
            }
        });

        new Handler().postDelayed(() -> {
            fab.setImageResource(R.drawable.ic_edit_black_24dp);
            fab.show();
        }, 200);

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.action_user);
        toolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_about:
                    new AboutDialogFragment().show(getFragmentManager(), null);
                    return true;

                case R.id.action_my_menu:
                    DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);

                    if (!drawer.isDrawerOpen(GravityCompat.END)) {
                        drawer.openDrawer(GravityCompat.END);
                    }
                    return true;
            }
            return false;
        });

        AppBarLayout appBarLayout = getActivity().findViewById(R.id.appBarLayout);
        lockAppBarLayoutScrolling(appBarLayout, false);

        //FIXME: Remove this mock
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, new BusinessMenusDetailFragment());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AUTH_CODE) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                intent.putExtra(DETAIL_FRAGMENT_CLASS, UserEditingFragment.class);
                startActivity(intent);
            } else if (response != null && response.getErrorCode() == ErrorCodes.NO_NETWORK && getView() != null) {
                Snackbar.make(getView(), R.string.no_internet_connection, Snackbar.LENGTH_LONG);
            } else if (response != null && response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR && getView() != null) {
                Snackbar.make(getView(), R.string.unknown_error, Snackbar.LENGTH_LONG);
            }
        }
    }
    //TODO: Show empty state screen based on auth state
    //TODO: Show user's diet info
}
