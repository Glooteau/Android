package com.rstex.glooteau.business.modelController;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.rstex.glooteau.business.model.Business;

import static com.rstex.glooteau.business.modelController.FirebasePaths.BUSINESS_PROFILE_IMAGE_STORAGE_PATH;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 18/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class BusinessModelController {
    private static final DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();
    private static final StorageReference mStorageReference = FirebaseStorage.getInstance().getReference();

    public static Business getBusiness(String key) {
        return new Business(key, "Lanchonete 2 Irmãos", "(16)3458-2347", "dois-irmãos@mail.com", "b0.jpg", true, null, null, null, null); //FIXME: Return async callback
    }

    public static StorageReference getProfileImage(Business business) {
        return mStorageReference.child(BUSINESS_PROFILE_IMAGE_STORAGE_PATH).child(business.getImageRef());
    }
}
