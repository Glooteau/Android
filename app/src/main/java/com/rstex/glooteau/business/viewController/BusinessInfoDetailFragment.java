package com.rstex.glooteau.business.viewController;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rstex.glooteau.R;
import com.rstex.glooteau.business.model.Business;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 19/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class BusinessInfoDetailFragment extends DetailFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_business_details_info, container, false);
        return view;
    }

    @Override
    public void viewWillAppear() {
        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_edit_black_24dp);
        fab.setOnClickListener(v -> mViewModel.setEditingState(true));
    }

    @Override
    public void modelDidChange(Business business) {
        //TODO: Set fields values
    }
}
