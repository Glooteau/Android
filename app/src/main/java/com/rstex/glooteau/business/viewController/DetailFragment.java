package com.rstex.glooteau.business.viewController;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.rstex.glooteau.business.model.Business;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 19/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class DetailFragment extends Fragment {
    protected BusinessDetailsContainerFragment.SharedViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(BusinessDetailsContainerFragment.SharedViewModel.class);
        mViewModel.getBusiness().observe(this, this::modelDidChange);
    }

    public abstract void modelDidChange(Business business);

    public abstract void viewWillAppear();
}
