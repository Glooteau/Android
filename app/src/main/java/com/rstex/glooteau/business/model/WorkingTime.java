package com.rstex.glooteau.business.model;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 08/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class WorkingTime {
    private String mOpeningTime;
    private String mClosingTime;

    public WorkingTime() {

    }

    public WorkingTime(String openingTime, String closingTime) {
        mOpeningTime = openingTime;
        mClosingTime = closingTime;
    }

    public String getOpeningTime() {
        return mOpeningTime;
    }

    public void setOpeningTime(String openingTime) {
        mOpeningTime = openingTime;
    }

    public String getClosingTime() {
        return mClosingTime;
    }

    public void setClosingTime(String closingTime) {
        mClosingTime = closingTime;
    }
}
