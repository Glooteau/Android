package com.rstex.glooteau.business.viewController;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rstex.glooteau.R;
import com.rstex.glooteau.business.model.Business;
import com.rstex.glooteau.business.modelController.BusinessModelController;
import com.rstex.glooteau.common.viewController.DetailsActivity;

import java.util.HashMap;

import static com.rstex.glooteau.common.utils.IntentConstants.DETAIL_FRAGMENT_CLASS;
import static com.rstex.glooteau.common.utils.IntentConstants.KEY;
import static com.rstex.glooteau.common.utils.LayoutUtils.lockAppBarLayoutScrolling;
import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 31/08/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class BusinessMainMapFragment extends Fragment {
    private static final int LOCATION_REQUEST_CODE = 5469;
    private static final int DETAILS_ACTIVITY_CODE = 1582;
    private GoogleMap mGoogleMap;


    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = layoutInflater.inflate(R.layout.fragment_business_main_map, viewGroup, false);

        final FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(v -> {
            //TODO: Recenter view
        });

        new Handler().postDelayed(() -> {
            fab.setImageResource(R.drawable.ic_store_mall_directory_black_24dp);
            fab.show();
        }, 200);

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.action_business_map);
        toolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_tags:
                    //TODO: Show tag filter list
                    return true;

                case R.id.action_following:
                    Toast.makeText(getContext(), R.string.action_following, Toast.LENGTH_SHORT).show();
                    //TODO: Show followed business on map
                    return true;

                case R.id.action_my_menu:
                    DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);

                    if (!drawer.isDrawerOpen(GravityCompat.END)) {
                        drawer.openDrawer(GravityCompat.END);
                    }
                    return true;
            }
            return false;
        });

        AppBarLayout appBarLayout = getActivity().findViewById(R.id.appBarLayout);
        lockAppBarLayoutScrolling(appBarLayout, true);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment mapFragment = SupportMapFragment.newInstance(new GoogleMapOptions().compassEnabled(true));
        getFragmentManager().beginTransaction().replace(R.id.mapLayout, mapFragment).commit();

        mapFragment.getMapAsync(googleMap -> {
            mGoogleMap = googleMap;

            tryEnableMyLocation();

            LatLngBounds bounds = new LatLngBounds.Builder().include(new LatLng(-21.982500, -47.882000)).build();//TODO: Include user?

            HashMap<LatLng, String> markers = new HashMap<>();
            markers.put(new LatLng(-21.983131, -47.882918), "Lanchonete Lima");
            markers.put(new LatLng(-21.982259, -47.881023), "Quiosque");
            markers.put(new LatLng(-21.982086, -47.884601), "Quiosque");

            for (LatLng marker : markers.keySet()) {
                googleMap.addMarker(new MarkerOptions().position(marker).title(markers.get(marker))).setTag("b1");//FIXME: setup properly
                bounds = bounds.including(marker);
            }

            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(-21.982500, -47.882000))
                    .title("2 Irmãos")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                    .draggable(true)).setTag("b0");

            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));

            googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View view = getLayoutInflater().inflate(R.layout.map_info_contents_business, null);

                    String key = nullableToString(marker.getTag());
                    Business business = BusinessModelController.getBusiness(key);

                    if (business == null) {
                        return null;
                    }

                    //FIXME: Image not being set with Glide
                    ImageView imageView = view.findViewById(R.id.imageView);
                    Glide.with(view)
                            .load(BusinessModelController.getProfileImage(business))
                            .apply(new RequestOptions().centerCrop())
                            .into(imageView);

                    TextView nameTextView = view.findViewById(R.id.nameTextView);
//                    nameTextView.setText(business.getName());
                    nameTextView.setText(marker.getTitle());

                    TextView followersTextView = view.findViewById(R.id.followersTextView);
                    followersTextView.setText("150");//TODO: Set text

                    TextView priceRangeTextView = view.findViewById(R.id.priceRangeTextView);
                    priceRangeTextView.setText("R$5,00 ~ R$80,00");//TODO: Set text

                    return view;
                }
            });

            googleMap.setOnInfoWindowClickListener(marker -> {
                Intent intent = new Intent(getContext(), DetailsActivity.class);
                intent.putExtra(DETAIL_FRAGMENT_CLASS, BusinessDetailsContainerFragment.class);
//                intent.putExtra(SCOPE, map); //TODO: Set this
                intent.putExtra(KEY, nullableToString(marker.getTag())); //FIXME: Set this
                startActivityForResult(intent, DETAILS_ACTIVITY_CODE);
            });

            final FloatingActionButton fab = getActivity().findViewById(R.id.fab);
            fab.setOnClickListener(v -> googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(-21.982500, -47.882000))));//FIXME: Set this

            googleMap.setOnMarkerClickListener(marker -> {
                fab.hide();
                return false;
            });

            googleMap.setOnInfoWindowCloseListener(marker -> fab.show());

            googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    //TODO: Set new user business position
                    Toast.makeText(getContext(), "Nova localização definida", Toast.LENGTH_SHORT).show();
                }
            });

            googleMap.setOnMyLocationClickListener(location -> {
                Snackbar.make(view, "Definir como localização atual do seu negócio?", Snackbar.LENGTH_LONG)
                        .setAction("Sim",
                                v -> Toast.makeText(getContext(), "Não foi possível alterar a localização", Toast.LENGTH_LONG).show())
                        .show();
                //TODO: Ask to set business location
            });
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_REQUEST_CODE) {
            tryEnableMyLocation();
        }
    }

    private void tryEnableMyLocation() {
        if ((ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) && mGoogleMap != null) {
            mGoogleMap.setMyLocationEnabled(true);
        }
    }
}
