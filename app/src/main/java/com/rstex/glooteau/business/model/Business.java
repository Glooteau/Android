package com.rstex.glooteau.business.model;

import com.google.firebase.database.Exclude;
import com.rstex.glooteau.common.model.FirebaseObject;

import java.util.HashMap;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 25/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class Business extends FirebaseObject {
    private String mName;
    private String mPhone;
    private String mEmail;
    private String mImageRef;
    private boolean mAcceptsOrder;
    private Location mLocation;
    private HashMap<String, Boolean> mMenu;
    private HashMap<String, WorkingTime> mWorkingTime;
    private HashMap<String, String> mAdditionalFields;

    public Business() {

    }

    public Business(String key, String name, String phone, String email, String imageRef, boolean acceptsOrder, Location location, HashMap<String, Boolean> menu, HashMap<String, WorkingTime> workingTime, HashMap<String, String> additionalFields) {
        super(key);
        mName = name;
        mPhone = phone;
        mEmail = email;
        mImageRef = imageRef;
        mAcceptsOrder = acceptsOrder;
        mLocation = location;
        mMenu = menu;
        mWorkingTime = workingTime;
        mAdditionalFields = additionalFields;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getImageRef() {
        return mImageRef;
    }

    public void setImageRef(String imageRef) {
        mImageRef = imageRef;
    }

    public boolean isAcceptsOrder() {
        return mAcceptsOrder;
    }

    public void setAcceptsOrder(boolean acceptsOrder) {
        mAcceptsOrder = acceptsOrder;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location location) {
        mLocation = location;
    }

    @Exclude
    public HashMap<String, Boolean> getMenu() {
        return mMenu;
    }

    public void setMenu(HashMap<String, Boolean> menu) {
        mMenu = menu;
    }

    public HashMap<String, WorkingTime> getWorkingTime() {
        return mWorkingTime;
    }

    public void setWorkingTime(HashMap<String, WorkingTime> workingTime) {
        mWorkingTime = workingTime;
    }

    public HashMap<String, String> getAdditionalFields() {
        return mAdditionalFields;
    }

    public void setAdditionalFields(HashMap<String, String> additionalFields) {
        mAdditionalFields = additionalFields;
    }
}
