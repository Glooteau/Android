package com.rstex.glooteau.business.viewController;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rstex.glooteau.R;
import com.rstex.glooteau.business.model.Business;
import com.rstex.glooteau.business.modelController.BusinessModelController;

import java.util.ArrayList;
import java.util.Arrays;

import static com.rstex.glooteau.common.utils.IntentConstants.KEY;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 19/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class BusinessDetailsContainerFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.viewpager, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        ViewPager viewPager = view.findViewById(R.id.viewPager);

        ArrayList<DetailFragment> detailsFragments = new ArrayList<>(
                Arrays.asList(new BusinessMenusDetailFragment(), new BusinessInfoDetailFragment()));
        ArrayList<DetailFragment> detailsEditingFragments = new ArrayList<>(
                Arrays.asList(new BusinessMenusEditingDetailFragment(), new BusinessInfoEditingDetailFragment()));

        PagerAdapter detailsAdapter = new FragmentStatePagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return detailsFragments.get(position);
            }

            @Override
            public int getCount() {
                return detailsEditingFragments.size();
            }
        };
        PagerAdapter detailsEditingAdapter = new FragmentStatePagerAdapter(getFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return detailsEditingFragments.get(position);
            }

            @Override
            public int getCount() {
                return detailsEditingFragments.size();
            }
        };

        SharedViewModel viewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        viewModel.setKey(getArguments().getString(KEY));
        viewModel.getEditingState().observe(this, isEditing -> {
            int position = viewModel.getTabPosition().getValue() != null ? viewModel.getTabPosition().getValue() : 0;
            viewPager.setAdapter(isEditing != null && isEditing ? detailsEditingAdapter : detailsAdapter);
            viewPager.setCurrentItem(position);
            updateView(viewPager, position);
        });
        viewModel.getTabPosition().observe(this, position -> {
            position = position != null ? position : 0;
            viewPager.setCurrentItem(position);
            updateView(viewPager, position);
        });
        viewModel.getBusiness().observe(this, business -> {
//            if (business == null || business.getKey().equals(UserModelController.getUserId())) {
//                fab.show();
//            } else {
//                fab.hide();
//            }
            //FIXME: Uncomment above code and remove image button and name mock [?]
            ImageButton imageButton = getActivity().findViewById(R.id.detailImageButton);
                    Glide.with(view)
                            .load(BusinessModelController.getProfileImage(business))
                            .apply(new RequestOptions().centerCrop())
                            .into(imageButton);
            Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
            if (business != null) toolbar.setTitle(business.getName());

            viewModel.postEditingState();
        });

        TabLayout tabLayout = getActivity().findViewById(R.id.tabBar);
        tabLayout.removeAllTabs();
        tabLayout.clearOnTabSelectedListeners();
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_local_library_white_24dp));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_info_outline_white_24dp));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewModel.setTabPosition(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                TabLayout.Tab tab = tabLayout.getTabAt(position);
                if (tab != null) {
                    tab.select();
                }
            }
        });
    }

    private void updateView(ViewPager viewPager, int position) {
        FragmentStatePagerAdapter adapter = (FragmentStatePagerAdapter) viewPager.getAdapter();
        DetailFragment fragment = (DetailFragment) adapter.getItem(position);
        new Handler().postDelayed(fragment::viewWillAppear, 200); //FIXME: Change to fragment callback
    }

    static class SharedViewModel extends ViewModel {
        private String mKey;
        private MutableLiveData<Boolean> mEditingState;
        private MutableLiveData<Integer> mTabPosition;
        private MutableLiveData<Business> mBusinessMutableLiveData;

        void setKey(String key) {
            mKey = key;
        }

        LiveData<Boolean> getEditingState() {
            if (mEditingState == null) {
                mEditingState = new MutableLiveData<>();
                mEditingState.setValue(false);
            }
            return mEditingState;
        }

        void setTabPosition(int position) {
            if (mTabPosition == null) {
                mTabPosition = new MutableLiveData<>();
            }
            mTabPosition.setValue(position);
        }

        LiveData<Integer> getTabPosition() {
            if (mTabPosition == null) {
                mTabPosition = new MutableLiveData<>();
                mTabPosition.setValue(0);
            }
            return mTabPosition;
        }

        void setEditingState(boolean state) {
            if (mEditingState == null) {
                mEditingState = new MutableLiveData<>();
            }
            mEditingState.setValue(state);
        }

        void postEditingState() {
            mEditingState.setValue(mEditingState.getValue());
        }

        LiveData<Business> getBusiness() {
            if (mBusinessMutableLiveData == null) {
                mBusinessMutableLiveData = new MutableLiveData<>();
                loadBusiness();
            }
            return mBusinessMutableLiveData;
        }

        private void loadBusiness() {
            Business business = BusinessModelController.getBusiness(mKey);
            mBusinessMutableLiveData.postValue(business); //TODO: Change to async callback
        }
    }
}
