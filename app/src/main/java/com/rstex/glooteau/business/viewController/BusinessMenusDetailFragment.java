package com.rstex.glooteau.business.viewController;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.rstex.glooteau.R;
import com.rstex.glooteau.business.model.Business;
import com.rstex.glooteau.common.view.MenuCardViewHolder;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.food.modelController.FoodModelController;
import com.rstex.glooteau.user.modelController.UserModelController;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 19/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class BusinessMenusDetailFragment extends DetailFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview_8dp_padding, container, false);

        //FIXME: DELETE ALL THIS MOCK
        DatabaseReference foodRef = FoodModelController.getFoodInfoReference();
        DatabaseReference menuIndexRef = UserModelController.getIndexReference(UserModelController.IndexScope.Menu);

        if (menuIndexRef == null) {
            return view;
        }

        FirebaseRecyclerOptions<Food> firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Food>()
                .setIndexedQuery(menuIndexRef, foodRef, Food.class)
                .setLifecycleOwner(this)
                .build();

        FirebaseRecyclerAdapter recyclerAdapter = new FirebaseRecyclerAdapter<Food, MenuCardViewHolder>(firebaseRecyclerOptions) {
            @Override
            public MenuCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new MenuCardViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.card_view_menu, parent, false));
            }

            @Override
            public void onBindViewHolder(MenuCardViewHolder holder, int position, final Food food) {
                food.setKey(getRef(position).getKey());
                holder.mTitle.setText("Almoço");
                holder.mPrice.setText("R$5,00 ~ R$35,00");
                holder.mDescription.setText(food.getDescription());
                holder.mTime.setText("10:30 ~ 14:00");
                holder.mTags.setText("Zero gluten; Zero lactose");
                holder.mWeekdaysView.setSelectedWeekdays(new ArrayList<>(Arrays.asList("Monday", "Tuesday", "Friday")));

                holder.itemView.setOnClickListener(view1 -> Toast.makeText(getContext(), "Cardápio vazio", Toast.LENGTH_LONG).show());
            }
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);

        return view;
    }

    @Override
    public void viewWillAppear() {
        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_edit_black_24dp);
        fab.setOnClickListener(v -> mViewModel.setEditingState(true));
    }

    @Override
    public void modelDidChange(Business business) {
        //TODO: Set fields values
    }
}
