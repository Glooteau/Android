package com.rstex.glooteau.business.modelController;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 15/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public interface FirebasePaths {
    //RealTimeDatabase
    String BUSINESS_PATH = "business";
    String BUSINESS_INFO_PATH = BUSINESS_PATH + "/info";
    String BUSINESS_MENU_PATH = BUSINESS_PATH + "/menu";
    String BUSINESS_FOOD_TAGS_PATH = BUSINESS_PATH + "/foodTags";
    String BUSINESS_WORKING_TIMES_PATH = BUSINESS_PATH + "/workingTimes";
    String BUSINESS_ADDITIONAL_FIELDS_PATH = BUSINESS_PATH + "/additionalFields";

    //Storage
    String BUSINESS_PROFILE_IMAGE_STORAGE_PATH = BUSINESS_PATH + "/profileImage";
}
