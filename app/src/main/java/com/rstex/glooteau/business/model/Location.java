package com.rstex.glooteau.business.model;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 08/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class Location {
    private double mLatitude;
    private double mLongitude;
    private String mAddress;

    public Location() {

    }

    public Location(double latitude, double longitude, String address) {
        mLatitude = latitude;
        mLongitude = longitude;
        mAddress = address;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }
}
