package com.rstex.glooteau.food.viewModel;

import android.support.annotation.NonNull;

import com.rstex.glooteau.food.model.Ingredient;
import com.rstex.glooteau.food.view.IngredientEditItemView;

import java.util.ArrayList;
import java.util.List;

import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;
import static com.rstex.glooteau.common.utils.Units.*;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 30/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class IngredientEditItemViewModel implements IngredientEditItemView.IngredientEditItemViewModel {
    private Ingredient mIngredient;

    public IngredientEditItemViewModel(@NonNull Ingredient ingredient) {
        mIngredient = ingredient;
    }

    @Override
    public String Title() {
        return nullableToString(mIngredient.getName());
    }

    @Override
    public String Amount() {
        return nullableToString(mIngredient.getAmount());
    }

    @Override
    public String SelectedUnit() {
        return mIngredient.getUnit();
    }

    @Override
    public List<String> UnitsList() {
        List<String> units = new ArrayList<>(MassUnits());
        units.addAll(LiquidVolumeUnits());
        units.addAll(QuantityUnits());
        return units;
    }
}
