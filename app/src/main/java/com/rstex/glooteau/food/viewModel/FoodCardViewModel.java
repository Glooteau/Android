package com.rstex.glooteau.food.viewModel;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.rstex.glooteau.R;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.food.view.FoodCardViewHolder;
import com.rstex.glooteau.user.modelController.UserModelController;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 21/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class FoodCardViewModel implements FoodCardViewHolder.FoodCardViewModel {
    protected final Food mFood;

    public FoodCardViewModel(Food food) {
        mFood = food;
    }

    @Override
    public String Title() {
        return mFood.getName();
    }

    @Override
    public String Description() {
        return mFood.getDescription();
    }

    @Override
    public String Price() {
        if (mFood.getCurrency() != null && mFood.getPrice() != 0) {
            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
            format.setCurrency(Currency.getInstance(mFood.getCurrency()));
            return format.format(mFood.getPrice());
        }
        return "";
    }

    @Override
    public void setImage(ImageView image) {
        StorageReference imageReference = FirebaseStorage.getInstance().getReference();
        if (mFood.getImageRef() != null && mFood.getImageRef().length() > 0) {
            imageReference = imageReference.child(mFood.getImageRef());
        }
        Glide.with(image.getContext())
                .load(imageReference)
                .apply(RequestOptions.centerCropTransform()
                        .placeholder(R.drawable.ic_more_horiz_grey_24dp)
                        .error(R.drawable.ic_restaurant_menu_grey_24dp))
                .into(image);
    }

    @Override
    public void setupButton1(final ImageButton button) {
        //TODO: Set visibility to gone and set visible on completion
        //FIXME: Check if already in bookmarks
        button.setImageResource(R.drawable.ic_bookmark_border_black_24dp);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserModelController.addBookmark(mFood);
                button.setImageResource(R.drawable.ic_bookmark_black_24dp);
            }
        });
    }

    @Override
    public void setupButton2(final ImageButton button) {
        //TODO: Check if already on menu
        button.setImageResource(R.drawable.ic_local_library_black_24dp);
        button.setOnClickListener(view -> {
//                button.setVisibility(View.GONE);
            UserModelController.addToMenu(mFood);
        });
    }

    @Override
    public void setupButton3(ImageButton button) {
        button.setVisibility(View.GONE);
//        button.setImageResource(R.drawable.ic_share_black_24dp);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //TODO: Open share bottom dialog
//            }
//        });
    }
}
