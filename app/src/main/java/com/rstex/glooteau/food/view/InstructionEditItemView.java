package com.rstex.glooteau.food.view;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import com.rstex.glooteau.R;
import com.rstex.glooteau.common.view.AdditiveItem;

import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 30/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class InstructionEditItemView extends ConstraintLayout implements AdditiveItem {
    private EditText mText;
    private OnFillListener mFillListener;
    private AttributeSet mAttrs;

    public InstructionEditItemView(Context context) {
        super(context);
        init(context);
    }

    public InstructionEditItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        mAttrs = attrs;
    }

    private void init(Context context) {
        View rootView = inflate(context, R.layout.item_view_instruction_edit, this);
        final InstructionEditItemView self = this;

        mText = rootView.findViewById(R.id.textEditText);
        mText.setHorizontallyScrolling(false);
        mText.setMaxLines(Integer.MAX_VALUE);
        mText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mFillListener != null) {
                    mFillListener.onFill(self);
                }
            }
        });
    }

    @Override
    public boolean isFilled() {
        return !getText().isEmpty();
    }

    @Override
    public void setOnFillListener(OnFillListener listener) {
        mFillListener = listener;
    }

    @Override
    public AdditiveItem newInstance() {
        if (mAttrs != null){
            return new InstructionEditItemView(getContext(), mAttrs);
        }
        return new InstructionEditItemView(getContext());
    }

    public void setText(String text) {
        mText.setText(nullableToString(text));

        invalidate();
        requestLayout();
    }

    public String getText() {
        return mText.getText().toString();
    }
}
