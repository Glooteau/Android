package com.rstex.glooteau.food.modelController;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.rstex.glooteau.common.utils.CallbackValueEventListener;
import com.rstex.glooteau.common.utils.SynchronizedCountdown;
import com.rstex.glooteau.food.model.Food;

import java.util.LinkedList;

import static com.rstex.glooteau.common.modelController.FirebasePaths.LIST_PAGE_SIZE;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 21/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

class IndexedFoodListFirebaseHandler {
    private final DatabaseReference mReference;
    private final LinkedList<Food> mPreviousList;

    IndexedFoodListFirebaseHandler(DatabaseReference reference, LinkedList<Food> previousList) {
        mReference = reference;
        mPreviousList = previousList;
    }

    void handleRequest(FoodListCallback callback) {
        Query query = mReference
                .limitToFirst(LIST_PAGE_SIZE)
                .startAt(mPreviousList.peekLast().getKey());

        query.addListenerForSingleValueEvent(new CallbackValueEventListener<FoodListCallback>(callback) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final LinkedList<Food> foodList = new LinkedList<>();
                final SynchronizedCountdown synchronizedSuccess = new SynchronizedCountdown(
                        () -> getCallback().onSuccess(
                                new FoodListCallback.FoodListResponse(
                                        foodList, mPreviousList.size(), foodList.size())), dataSnapshot.getChildrenCount());

                for (final DataSnapshot foodSnapshot : dataSnapshot.getChildren()) {
                    new FoodInfoFirebaseHandler().handleRequest(new FoodCallback(foodSnapshot.getKey()) {
                        @Override
                        public void onSuccess(Food food) {
                            foodList.add(food);
                            synchronizedSuccess.decrement();
                        }

                        @Override
                        public void onError(Error error) {
                            synchronizedSuccess.decrement();
                        }
                    });
                }
            }
        });
    }
}
