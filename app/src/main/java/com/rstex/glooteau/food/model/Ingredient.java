package com.rstex.glooteau.food.model;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 08/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class Ingredient {
    private String mName;
    private String mUnit;
    private double mAmount;

    public Ingredient() {

    }

    public Ingredient(String name, String unit, double amount) {
        mName = name;
        mUnit = unit;
        mAmount = amount;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(String unit) {
        mUnit = unit;
    }

    public double getAmount() {
        return mAmount;
    }

    public void setAmount(double amount) {
        mAmount = amount;
    }
}
