package com.rstex.glooteau.food.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.rstex.glooteau.R;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 06/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class FoodCardViewHolder extends RecyclerView.ViewHolder {
    public interface FoodCardViewModel {
        String Title();
        String Description();
        String Price();
        void setImage(ImageView image);
        void setupButton1(ImageButton button);
        void setupButton2(ImageButton button);
        void setupButton3(ImageButton button);
    }

    private final TextView mTitle;
    private final TextView mDescription;
    private final TextView mPrice;
    private final ImageView mImageView;
    private final ImageButton mButton1;
    private final ImageButton mButton2;
    private final ImageButton mButton3;

    public FoodCardViewHolder(View itemView) {
        super(itemView);
        mTitle = itemView.findViewById(R.id.title);
        mDescription = itemView.findViewById(R.id.description);
        mPrice = itemView.findViewById(R.id.price);
        mImageView = itemView.findViewById(R.id.detail_image);
        mButton1 = itemView.findViewById(R.id.imageButton);
        mButton2 = itemView.findViewById(R.id.imageButton2);
        mButton3 = itemView.findViewById(R.id.imageButton3);
    }

    public void setViewModel(@NonNull FoodCardViewModel viewModel) {
        mTitle.setText(viewModel.Title());
        mDescription.setText(viewModel.Description());
        mPrice.setText(viewModel.Price());
        viewModel.setImage(mImageView);
        viewModel.setupButton1(mButton1);
        viewModel.setupButton2(mButton2);
        viewModel.setupButton3(mButton3);
    }
}
