package com.rstex.glooteau.food.modelController;

import com.rstex.glooteau.common.utils.Callback;
import com.rstex.glooteau.food.model.Food;

import java.util.LinkedList;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 20/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public interface FoodListCallback extends Callback<FoodListCallback.FoodListResponse> {

    class FoodListResponse {
        final private LinkedList<Food> mFoodList;
        final private Integer mRangeStart;
        final private Integer mInsertedCount;

        FoodListResponse(LinkedList<Food> foodList, Integer rangeStart, Integer insertedCount) {
            mFoodList = foodList;
            mRangeStart = rangeStart;
            mInsertedCount = insertedCount;
        }

        public LinkedList<Food> getFoodList() {
            return mFoodList;
        }

        public Integer getRangeStart() {
            return mRangeStart;
        }

        public Integer getInsertedCount() {
            return mInsertedCount;
        }
    }

}
