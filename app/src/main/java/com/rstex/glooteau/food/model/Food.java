package com.rstex.glooteau.food.model;

import android.net.Uri;

import com.google.firebase.database.Exclude;
import com.rstex.glooteau.common.model.FirebaseObject;

import java.util.List;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 06/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class Food extends FirebaseObject {
    private String mOwnerId;
    private String mName;
    private String mDescription;
    private String mImageRef;
    private String mCurrency;
    private double mPrice;
    private List<Ingredient> mIngredients;
    private List<String> mInstructions;
    private List<String> mTags;
    private Uri mImageTempUri;

    public Food() {

    }

    public Food(String key, String ownerId, String name, String description, String imageRef, String currency, double price, List<Ingredient> ingredients, List<String> instructions, List<String> tags, Uri imageTempUri) {
        super(key);
        mOwnerId = ownerId;
        mName = name;
        mDescription = description;
        mImageRef = imageRef;
        mCurrency = currency;
        mPrice = price;
        mIngredients = ingredients;
        mInstructions = instructions;
        mTags = tags;
        mImageTempUri = imageTempUri;
    }

    public String getOwnerId() {
        return mOwnerId;
    }

    public void setOwnerId(String ownerId) {
        mOwnerId = ownerId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getImageRef() {
        return mImageRef;
    }

    public void setImageRef(String imageRef) {
        mImageRef = imageRef;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public double getPrice() {
        return mPrice;
    }

    public void setPrice(double price) {
        mPrice = price;
    }

    @Exclude
    public List<Ingredient> getIngredients() {
        return mIngredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        mIngredients = ingredients;
    }

    @Exclude
    public List<String> getInstructions() {
        return mInstructions;
    }

    public void setInstructions(List<String> instructions) {
        mInstructions = instructions;
    }

    @Exclude
    public List<String> getTags() {
        return mTags;
    }

    public void setTags(List<String> tags) {
        mTags = tags;
    }

    @Exclude
    public Uri getImageTempUri() {
        return mImageTempUri;
    }

    public void setImageTempUri(Uri imageTempUri) {
        mImageTempUri = imageTempUri;
    }
}
