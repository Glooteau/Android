package com.rstex.glooteau.food.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.rstex.glooteau.R;
import com.rstex.glooteau.common.view.AdditiveItem;

import java.util.ArrayList;
import java.util.List;

import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;
import static com.rstex.glooteau.common.utils.Units.LiquidVolumeUnits;
import static com.rstex.glooteau.common.utils.Units.MassUnits;
import static com.rstex.glooteau.common.utils.Units.QuantityUnits;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 06/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class IngredientEditItemView extends ConstraintLayout implements AdditiveItem {
    public interface IngredientEditItemViewModel {
        String Title();
        String Amount();
        String SelectedUnit();
        List<String> UnitsList();
    }

    private EditText mTitle;
    private EditText mAmount;
    private Spinner mUnit;
    private OnFillListener mFillListener;
    private AttributeSet mAttrs;

    public IngredientEditItemView(Context context) {
        super(context);
        init(context);
    }

    public IngredientEditItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        mAttrs = attrs;
    }

    private void init(Context context) {
        View rootView = inflate(context, R.layout.item_view_ingredient_edit, this);
        final IngredientEditItemView self = this;

        mTitle = rootView.findViewById(R.id.titleEditText);
        mAmount = rootView.findViewById(R.id.amountEditText);
        mUnit = rootView.findViewById(R.id.unitSpinner);

        List<String> units = new ArrayList<>(MassUnits());
        units.addAll(LiquidVolumeUnits());
        units.addAll(QuantityUnits());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, units);
        mUnit.setAdapter(adapter);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mFillListener != null) {
                    mFillListener.onFill(self);
                }
            }
        };

        mTitle.addTextChangedListener(textWatcher);
        mAmount.addTextChangedListener(textWatcher);
    }

    public void setViewModel(@NonNull IngredientEditItemViewModel viewModel) {
        mTitle.setText(nullableToString(viewModel.Title()));
        mAmount.setText(nullableToString(viewModel.Amount()));

        if (viewModel.UnitsList() != null) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),
                    android.R.layout.simple_spinner_item, viewModel.UnitsList());
            mUnit.setAdapter(adapter);

            if (viewModel.UnitsList().contains(viewModel.SelectedUnit())) {
                mUnit.setSelection(viewModel.UnitsList().indexOf(viewModel.SelectedUnit()));
            }
        }

        invalidate();
        requestLayout();
    }

    @Override
    public boolean isFilled() {
        return !getTitle().isEmpty() &&
                !getAmount().isEmpty() &&
                !getUnit().isEmpty();
    }

    @Override
    public void setOnFillListener(OnFillListener listener) {
        mFillListener = listener;
    }

    @Override
    public AdditiveItem newInstance() {
        if (mAttrs != null){
            return new IngredientEditItemView(getContext(), mAttrs);
        }
        return new IngredientEditItemView(getContext());
    }

    public String getTitle() {
        return mTitle.getText().toString();
    }

    public String getAmount() {
        return mAmount.getText().toString();
    }

    public String getUnit() {
        return nullableToString(mUnit.getSelectedItem());
    }
}
