package com.rstex.glooteau.food.view;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.rstex.glooteau.common.view.AdditiveItem;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 30/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class InstructionItemView extends AppCompatTextView implements AdditiveItem {
    private AttributeSet mAttrs;

    public InstructionItemView(Context context) {
        super(context);
        init();
    }

    public InstructionItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        mAttrs = attrs;
    }

    private void init() {
        this.setMaxLines(Integer.MAX_VALUE);
    }

    @Override
    public boolean isFilled() {
        return false;
    }

    @Override
    public void setOnFillListener(OnFillListener listener) {

    }

    @Override
    public AdditiveItem newInstance() {
        if (mAttrs != null){
            return new InstructionItemView(getContext(), mAttrs);
        }
        return new InstructionItemView(getContext());
    }
}
