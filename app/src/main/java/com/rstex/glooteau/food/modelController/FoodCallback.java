package com.rstex.glooteau.food.modelController;

import android.support.annotation.NonNull;

import com.rstex.glooteau.common.utils.Callback;
import com.rstex.glooteau.food.model.Food;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 20/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class FoodCallback implements Callback<Food> {
    private final String mKey;
    private Food food;

    public FoodCallback(@NonNull String key) {
        this.mKey = key;
    }

    public String getKey() {
        return mKey;
    }

    protected Food getFood() {
        return food;
    }

    protected void setFood(Food food) {
        this.food = food;
    }
}
