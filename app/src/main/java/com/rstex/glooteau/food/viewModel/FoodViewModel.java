package com.rstex.glooteau.food.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.food.modelController.FoodCallback;
import com.rstex.glooteau.food.modelController.FoodModelController;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 16/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class FoodViewModel extends ViewModel {
    private MutableLiveData<Food> mFood;

    public LiveData<Food> getFood(String foodKey) {
        if (mFood == null) {
            mFood = new MutableLiveData<>();
            loadFood(foodKey);
        }
        return mFood;
    }

    public void setFood(Food food) {
        mFood.setValue(food);
    }

    private void loadFood(String foodKey) {
        if (foodKey != null) {
            FoodModelController.getFood(FoodModelController.DetailLevel.DETAILED, new FoodCallback(foodKey) {
                @Override
                public void onSuccess(Food response) {
                    mFood.postValue(response);
                }

                @Override
                public void onError(Error error) {
                    mFood.postValue(null);
                }
            });
        } else {
            mFood.postValue(null);
        }
    }
}
