package com.rstex.glooteau.food.viewController;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.rstex.glooteau.R;
import com.rstex.glooteau.common.view.AutoAddingLinearLayout;
import com.rstex.glooteau.common.viewController.DetailsActivity;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.food.model.Ingredient;
import com.rstex.glooteau.food.modelController.FoodCallback;
import com.rstex.glooteau.food.modelController.FoodModelController;
import com.rstex.glooteau.food.view.IngredientItemView;
import com.rstex.glooteau.food.view.InstructionItemView;
import com.rstex.glooteau.food.viewModel.IngredientItemViewModel;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.rstex.glooteau.common.utils.IntentConstants.DETAIL_FRAGMENT_CLASS;
import static com.rstex.glooteau.common.utils.IntentConstants.KEY;
import static com.rstex.glooteau.common.utils.IntentConstants.SCOPE;
import static com.rstex.glooteau.user.modelController.UserModelController.getUserId;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 15/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class FoodDetailsFragment extends Fragment {
    private static final int EDIT_ACTIVITY_CODE = 1526;

    private View mView;
    private ImageButton mDetailImageButton;
    private ProgressBar mProgressBar;
    private TextView mNameTextView;
    private TextView mDescriptionTextView;
    private TextView mPriceTextView;
    private AutoAddingLinearLayout mIngredientLinearLayout;
    private AutoAddingLinearLayout mInstructionLinearLayout;

    private String mFoodKey;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_food_details, container, false);

        mDetailImageButton = getActivity().findViewById(R.id.detailImageButton);
        mProgressBar = mView.findViewById(R.id.progressBar);
        mNameTextView = mView.findViewById(R.id.nameTextView);
        mDescriptionTextView = mView.findViewById(R.id.descriptionTextView);
        mPriceTextView = mView.findViewById(R.id.priceTextView);
        mIngredientLinearLayout = mView.findViewById(R.id.ingredientLinearLayout);
        mInstructionLinearLayout = mView.findViewById(R.id.instructionLinearLayout);

        mFoodKey = getArguments().getString(KEY);
        loadViews();

        return mView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_ACTIVITY_CODE) {
            loadViews();
        }
    }

    private void loadViews() {
        if (mFoodKey == null) {
            throw new AssertionError("Null key");
        }

        mProgressBar.setVisibility(VISIBLE);

        final FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.hide();
        fab.setImageResource(R.drawable.ic_edit_black_24dp);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), DetailsActivity.class);
            intent.putExtra(DETAIL_FRAGMENT_CLASS, FoodDetailsEditingFragment.class);
            intent.putExtra(SCOPE, getArguments().getSerializable(SCOPE));
            intent.putExtra(KEY, mFoodKey);
            startActivityForResult(intent, EDIT_ACTIVITY_CODE);
        });

        final Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.getMenu().clear();

        FoodModelController.getFood(FoodModelController.DetailLevel.DETAILED, new FoodCallback(mFoodKey) {
            @Override
            public void onSuccess(final Food response) {
                if (getContext() == null) {
                    return;
                }

                toolbar.setTitle(response.getName());

                if (response.getOwnerId().equals(getUserId())) {
                    fab.show();

                    toolbar.inflateMenu(R.menu.action_food_details);
                    toolbar.setOnMenuItemClickListener(item -> {
                        if (item.getItemId() == R.id.action_delete) {
                            deleteFood(response);
                            return true;
                        }
                        return false;
                    });
                }

                if (response.getImageRef() != null && response.getImageRef().length() > 0) {
                    StorageReference imageReference = FirebaseStorage.getInstance().getReference().child(response.getImageRef());

                    Glide.with(getContext())
                            .load(imageReference)
                            .apply(RequestOptions.centerCropTransform()
                                    .placeholder(R.drawable.ic_more_horiz_white_24dp)
                                    .error(R.drawable.ic_restaurant_menu_white_24dp))
                            .into(mDetailImageButton);

                    mDetailImageButton.setOnClickListener(view -> {
                        //TODO: Open fullscreen image
                    });
                }

                mProgressBar.setVisibility(GONE);
//                mNameTextView.setText(response.getName()); //TODO: Remove from layout

                String price = "";
                if (response.getCurrency() != null && response.getPrice() != 0) {
                    NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
                    format.setCurrency(Currency.getInstance(response.getCurrency()));
                    price = format.format(response.getPrice());
                }
                mPriceTextView.setText(price);
                mPriceTextView.setVisibility(price.isEmpty() ? GONE : VISIBLE);

                mDescriptionTextView.setText(response.getDescription());
                mDescriptionTextView.setVisibility(response.getDescription().isEmpty() ? GONE : VISIBLE);
                mView.findViewById(R.id.descriptionImageView).setVisibility(response.getDescription().isEmpty() ? GONE : VISIBLE);

                for (Ingredient ingredient : response.getIngredients()) {
                    IngredientItemView itemView = new IngredientItemView(getContext());
                    itemView.setViewModel(new IngredientItemViewModel(ingredient));
                    mIngredientLinearLayout.addView(itemView);
                }
                mIngredientLinearLayout.setVisibility(response.getIngredients().isEmpty() ? GONE : VISIBLE);
                mView.findViewById(R.id.ingredientsTextView).setVisibility(response.getIngredients().isEmpty() ? GONE : VISIBLE);
                mView.findViewById(R.id.ingredientsDivider).setVisibility(response.getIngredients().isEmpty() ? GONE : VISIBLE);

                for (String instruction : response.getInstructions()) {
                    if (instruction == null || instruction.isEmpty()) {
                        continue;
                    }

                    InstructionItemView itemView = new InstructionItemView(getContext());
                    itemView.setText(instruction);
                    mInstructionLinearLayout.addView(itemView);
                }
                mInstructionLinearLayout.setVisibility(response.getInstructions().isEmpty() ? GONE : VISIBLE);
                mView.findViewById(R.id.instructionsTextView).setVisibility(response.getInstructions().isEmpty() ? GONE : VISIBLE);
                mView.findViewById(R.id.instructionsDivider).setVisibility(response.getInstructions().isEmpty() ? GONE : VISIBLE);
            }

            @Override
            public void onError(Error error) {
                if (getContext() == null) {
                    return;
                }
                Toast.makeText(getContext(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                getActivity().finish();
            }
        });
    }

    private void deleteFood(final Food food) {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.food_delete)
                .setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                    FoodModelController.deleteFood(food);
                    getActivity().finish();
                })
                .setNegativeButton(R.string.cancel, null)
                .show();
    }
}
