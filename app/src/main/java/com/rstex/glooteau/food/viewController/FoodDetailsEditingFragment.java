package com.rstex.glooteau.food.viewController;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.rstex.glooteau.R;
import com.rstex.glooteau.common.utils.ImageUtils;
import com.rstex.glooteau.common.utils.SynchronizedCountdown;
import com.rstex.glooteau.common.view.AdditiveItem;
import com.rstex.glooteau.common.view.AutoAddingLinearLayout;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.food.model.Ingredient;
import com.rstex.glooteau.food.modelController.FoodModelController;
import com.rstex.glooteau.food.view.IngredientEditItemView;
import com.rstex.glooteau.food.view.InstructionEditItemView;
import com.rstex.glooteau.food.viewModel.FoodViewModel;
import com.rstex.glooteau.food.viewModel.IngredientEditItemViewModel;

import java.util.ArrayList;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.rstex.glooteau.R.id.instructionLinearLayout;
import static com.rstex.glooteau.common.utils.IntentConstants.KEY;
import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;
import static com.rstex.glooteau.common.utils.Units.AllCurrencies;
import static com.rstex.glooteau.common.utils.Units.CurrencySymbols;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 15/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class FoodDetailsEditingFragment extends Fragment {
    private static final int CAMERA_PERMISSION_CODE = 1629;
    private static final int IMAGE_REQUEST_CODE = 6894;

    private ImageButton mDetailImageButton;
    private ProgressBar mProgressBar;
    private EditText mNameEditText;
    private EditText mDescriptionEditText;
    private EditText mAmountEditText;
    private Spinner mCurrencySpinner;
    private AutoAddingLinearLayout mIngredientLinearLayout;
    private AutoAddingLinearLayout mInstructionLinearLayout;
    private FloatingActionButton mFAB;

    private Food mFood;
    private Uri mImageUri;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_food_details_editing, container, false);
        final String foodKey = getArguments().getString(KEY);

        mDetailImageButton = getActivity().findViewById(R.id.detailImageButton);
        mFAB = getActivity().findViewById(R.id.fab);

        mProgressBar = view.findViewById(R.id.progressBar);
        mNameEditText = view.findViewById(R.id.nameEditText);
        mDescriptionEditText = view.findViewById(R.id.descriptionEditText);
        mAmountEditText = view.findViewById(R.id.amountEditText);
        mCurrencySpinner = view.findViewById(R.id.currencySpinner);
        mIngredientLinearLayout = view.findViewById(R.id.ingredientLinearLayout);
        mInstructionLinearLayout = view.findViewById(instructionLinearLayout);

        mDetailImageButton.setOnClickListener(v -> pickImage());

        mFAB.setImageResource(R.drawable.ic_save_white_24dp);
        mFAB.setOnClickListener(v -> persistFood());

        mCurrencySpinner.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, CurrencySymbols()));
        if (CurrencySymbols().contains(Currency.getInstance(Locale.getDefault()).getSymbol())) {
            mCurrencySpinner.setSelection(CurrencySymbols().indexOf(Currency.getInstance(Locale.getDefault()).getSymbol()));
        }

        mProgressBar.setVisibility(View.VISIBLE);
        mDetailImageButton.setImageResource(R.drawable.ic_add_a_photo_white_24dp);
        mIngredientLinearLayout.addView(new IngredientEditItemView(getContext()));
        mInstructionLinearLayout.addView(new InstructionEditItemView(getContext()));

        FoodViewModel viewModel = ViewModelProviders.of(getActivity()).get(FoodViewModel.class);
        viewModel.getFood(foodKey).observe(this, food -> {
            if (food != null) {
                mFood = food;
                loadViews();
            } else if (foodKey != null) {
                getActivity().finish();
            } else {
                food = new Food();
                food.setIngredients(new LinkedList<>());
                food.setInstructions(new LinkedList<>());
                food.setTags(new LinkedList<>());
                viewModel.setFood(food);
            }
            mProgressBar.setVisibility(View.GONE);
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                mFood.setImageTempUri(data.getData());
            } else {
                mFood.setImageTempUri(mImageUri);
            }

            if (getContext() != null) {
                Glide.with(getContext())
                        .load(mFood.getImageTempUri())
                        .apply(RequestOptions.centerCropTransform()
                                .placeholder(R.drawable.ic_more_horiz_white_24dp)
                                .error(R.drawable.ic_restaurant_menu_white_24dp))
                        .into(mDetailImageButton);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_CODE) {
            mImageUri = ImageUtils.pickImage(this, CAMERA_PERMISSION_CODE, IMAGE_REQUEST_CODE,
                    grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        updateModel();
    }

    private void pickImage() {
        if (mFood.getImageRef() != null && !mFood.getImageRef().isEmpty() ||
                mFood.getImageTempUri() != null) {
            PopupMenu menu = new PopupMenu(getContext(), mDetailImageButton);
            menu.inflate(R.menu.image_operations);
            menu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.image_replace:
                        mImageUri = ImageUtils.pickImage(FoodDetailsEditingFragment.this, CAMERA_PERMISSION_CODE, IMAGE_REQUEST_CODE, true);
                        return true;

                    case R.id.image_remove:
                        mFood.setImageRef(null);
                        mFood.setImageTempUri(null);
                        mDetailImageButton.setImageResource(R.drawable.ic_add_a_photo_white_24dp);
                        mImageUri = null;
                        return true;

                    default:
                        return false;
                }
            });
            menu.show();
        } else {
            mImageUri = ImageUtils.pickImage(FoodDetailsEditingFragment.this, CAMERA_PERMISSION_CODE, IMAGE_REQUEST_CODE, true);
        }
    }

    private void loadViews() {
        StorageReference imageReference = FirebaseStorage.getInstance().getReference();
        if (mFood.getImageRef() != null && mFood.getImageRef().length() > 0) {
            imageReference = imageReference.child(mFood.getImageRef());
        }

        Glide.with(getContext())
                .load(imageReference)
                .apply(RequestOptions.centerCropTransform()
                        .placeholder(R.drawable.ic_more_horiz_white_24dp)
                        .error(R.drawable.ic_restaurant_menu_white_24dp))
                .into(mDetailImageButton);

        mNameEditText.setText(mFood.getName());
        mDescriptionEditText.setText(mFood.getDescription());
        mAmountEditText.setText(String.valueOf(mFood.getPrice()));

        if (CurrencySymbols().contains(mFood.getCurrency())) {
            mCurrencySpinner.setSelection(CurrencySymbols().indexOf(mFood.getCurrency()));
        }

        mIngredientLinearLayout.removeAllViews();
        mInstructionLinearLayout.removeAllViews();

        for (Ingredient ingredient : mFood.getIngredients()) {
            IngredientEditItemView itemView = new IngredientEditItemView(getContext());
            itemView.setViewModel(new IngredientEditItemViewModel(ingredient));
            mIngredientLinearLayout.addView(itemView);
        }

        for (String instruction : mFood.getInstructions()) {
            InstructionEditItemView itemView = new InstructionEditItemView(getContext());
            itemView.setText(instruction);
            mInstructionLinearLayout.addView(itemView);
        }

        mIngredientLinearLayout.addView(new IngredientEditItemView(getContext()));
        mInstructionLinearLayout.addView(new InstructionEditItemView(getContext()));
    }

    private void updateModel() {
        mFood.setName(mNameEditText.getText().toString());
        mFood.setDescription(mDescriptionEditText.getText().toString());

        if (!mAmountEditText.getText().toString().isEmpty()) {
            mFood.setPrice(Double.valueOf(mAmountEditText.getText().toString()));
        } else {
            mFood.setPrice(0);
        }
        for (Currency currency : AllCurrencies()) {
            if (currency.getSymbol().equalsIgnoreCase(nullableToString(mCurrencySpinner.getSelectedItem()))) {
                mFood.setCurrency(currency.getCurrencyCode());
                break;
            }
        }

        List<Ingredient> ingredients = new ArrayList<>();
        for (AdditiveItem item : mIngredientLinearLayout.getAdditiveItems()) {
            IngredientEditItemView itemView = ((IngredientEditItemView) item);

            if (itemView.isFilled()) {
                ingredients.add(new Ingredient(itemView.getTitle(),
                        itemView.getUnit(), Double.parseDouble(itemView.getAmount())));
            }
        }
        mFood.setIngredients(ingredients);

        List<String> instructions = new ArrayList<>();
        for (AdditiveItem item : mInstructionLinearLayout.getAdditiveItems()) {
            InstructionEditItemView itemView = (InstructionEditItemView) item;

            if (itemView.isFilled()) {
                instructions.add(itemView.getText());
            }
        }
        mFood.setInstructions(instructions);
    }

    private void persistFood() {
        updateModel();

        if (mNameEditText.getText().toString().isEmpty()) {
            mNameEditText.setError(getString(R.string.food_name_error));
            mNameEditText.requestFocus();
            return;
        }

        mProgressBar.setIndeterminate(true);
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.requestFocus();

        mFAB.hide();

        Long taskNumber = mFood.getImageTempUri() == null ? 1L : 2L;

        final SynchronizedCountdown synchronizedCompletion = new SynchronizedCountdown(() -> {
            if (getActivity() != null) {
                getActivity().finish();
            }
        }, taskNumber);

        final SynchronizedCountdown synchronizedProgressBarHide = new SynchronizedCountdown(
                () -> mProgressBar.setVisibility(View.GONE), taskNumber);

        FoodModelController.saveFood(mFood,
                aVoid -> {
                    synchronizedCompletion.decrement();
                    synchronizedProgressBarHide.decrement();
                },
                exception -> {
                    synchronizedProgressBarHide.decrement();
                    mFAB.show();
                    if (getView() != null) {
                        Snackbar.make(getView(), exception.getLocalizedMessage(), Snackbar.LENGTH_LONG)
                                .setAction(R.string.retry, view -> persistFood()).show();
                    }
                    FirebaseCrash.report(exception);
                }, taskSnapshot -> {
                    if (taskSnapshot.getTotalByteCount() == 0 || taskSnapshot.getBytesTransferred() == 0) {
                        return;
                    }
                    mProgressBar.setIndeterminate(false);
                    mProgressBar.setMax((int) taskSnapshot.getTotalByteCount());
                    mProgressBar.setProgress((int) taskSnapshot.getBytesTransferred());
                }, taskSnapshot -> {
                    synchronizedCompletion.decrement();
                    synchronizedProgressBarHide.decrement();
                    mProgressBar.setIndeterminate(true);
                }, exception -> {
                    synchronizedProgressBarHide.decrement();
                    mProgressBar.setIndeterminate(true);
                    mFAB.show();
                    if (getView() != null) {
                        Snackbar.make(getView(), exception.getLocalizedMessage(), Snackbar.LENGTH_LONG)
                                .setAction(R.string.retry, view -> persistFood()).show();
                    }
                    FirebaseCrash.report(exception);
                });
    }
}
