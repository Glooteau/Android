package com.rstex.glooteau.food.viewController;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.rstex.glooteau.R;
import com.rstex.glooteau.common.utils.IntentConstants;
import com.rstex.glooteau.common.view.FABScrollListener;
import com.rstex.glooteau.common.viewController.DetailsActivity;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.food.modelController.FoodModelController;
import com.rstex.glooteau.food.view.FoodCardViewHolder;
import com.rstex.glooteau.food.viewModel.FoodCardViewModel;

import static com.rstex.glooteau.common.utils.IntentConstants.DETAIL_FRAGMENT_CLASS;
import static com.rstex.glooteau.common.utils.IntentConstants.KEY;
import static com.rstex.glooteau.common.utils.IntentConstants.SCOPE;
import static com.rstex.glooteau.common.utils.LayoutUtils.lockAppBarLayoutScrolling;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 25/08/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class FoodMainFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview_8dp_padding, container, false);

        final FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(v -> {
            //TODO: Add new food
        });

        new Handler().postDelayed(() -> {
            fab.setImageResource(R.drawable.ic_explore_black_24dp);
            fab.show();
        }, 200);

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.action_food);
        toolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_tags:
                    //TODO: Show tag filter list
                    return true;

                case R.id.action_bookmark:
                    Toast.makeText(getContext(), R.string.action_bookmark, Toast.LENGTH_SHORT).show();
                    //TODO: Show bookmarked foods
                    return true;

                case R.id.action_my_menu:
                    DrawerLayout drawer = getActivity().findViewById(R.id.drawer_layout);

                    if (!drawer.isDrawerOpen(GravityCompat.END)) {
                        drawer.openDrawer(GravityCompat.END);
                    }
                    return true;
            }
            return false;
        });

        AppBarLayout appBarLayout = getActivity().findViewById(R.id.appBarLayout);
        lockAppBarLayoutScrolling(appBarLayout, true);

        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);

        FirebaseRecyclerOptions<Food> firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Food>()
                .setQuery(FoodModelController.getFoodInfoReference(), Food.class)
                .setLifecycleOwner(this)
                .build();

        final FirebaseRecyclerAdapter recyclerAdapter = new FirebaseRecyclerAdapter<Food, FoodCardViewHolder>(firebaseRecyclerOptions) {
            @Override
            public FoodCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new FoodCardViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.card_view_food, parent, false));
            }

            @Override
            public void onBindViewHolder(FoodCardViewHolder holder, int position, final Food food) {
                food.setKey(getRef(position).getKey());
                holder.setViewModel(new FoodCardViewModel(food));

                holder.itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(getContext(), DetailsActivity.class);
                    intent.putExtra(DETAIL_FRAGMENT_CLASS, FoodDetailsFragment.class);
                    intent.putExtra(SCOPE, IntentConstants.Scope.Feed);
                    intent.putExtra(KEY, food.getKey());
                    startActivity(intent);
                });
            }
        };

        FloatingActionButton fab = getActivity().findViewById(R.id.fab);

        final RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        recyclerView.addOnScrollListener(new FABScrollListener(fab));
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        new Handler().postDelayed(() -> recyclerView.setAdapter(recyclerAdapter), 180);
    }
}
