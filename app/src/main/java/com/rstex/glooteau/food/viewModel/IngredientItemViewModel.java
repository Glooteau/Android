package com.rstex.glooteau.food.viewModel;

import android.support.annotation.NonNull;

import com.rstex.glooteau.R;
import com.rstex.glooteau.food.model.Ingredient;
import com.rstex.glooteau.food.view.IngredientItemView;

import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;
import static com.rstex.glooteau.common.utils.Units.LiquidVolumeUnits;
import static com.rstex.glooteau.common.utils.Units.MassUnits;
import static com.rstex.glooteau.common.utils.Units.QuantityUnits;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 30/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class IngredientItemViewModel implements IngredientItemView.IngredientItemViewModel {
    private Ingredient mIngredient;

    public IngredientItemViewModel(@NonNull Ingredient ingredient) {
        mIngredient = ingredient;
    }

    @Override
    public String Title() {
        return nullableToString(mIngredient.getName());
    }

    @Override
    public String Amount() {
        return nullableToString(mIngredient.getAmount());
    }

    @Override
    public String Unit() {
        return mIngredient.getUnit();
    }

    @Override
    public int UnitImage() {
        int resource = 0;
        if (MassUnits().contains(mIngredient.getUnit())) {
            resource = R.drawable.ic_landscape_black_24dp;
        } else if (LiquidVolumeUnits().contains(mIngredient.getUnit())) {
            resource = R.drawable.ic_local_drink_black_24dp;
        } else if (QuantityUnits().contains(mIngredient.getUnit())) {
            resource = R.drawable.ic_grain_black_24dp;
        }
        return resource;
    }
}
