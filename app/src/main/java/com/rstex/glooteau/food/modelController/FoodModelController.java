package com.rstex.glooteau.food.modelController;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rstex.glooteau.common.utils.ChainedHandler;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.user.modelController.UserModelController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_IMAGE_STORAGE_PATH;
import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_INFO_PATH;
import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_INGREDIENTS_PATH;
import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_INSTRUCTIONS_PATH;
import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_TAGS_PATH;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 12/09/17.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class FoodModelController {
    private static final DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();
    private static final StorageReference mStorageReference = FirebaseStorage.getInstance().getReference();

    public enum DetailLevel {
        BASIC,
        DETAILED
    }

    public static void getFood(DetailLevel level, FoodCallback callback) {
        switch (level) {
            case BASIC:
                getFoodInfo(null).handleRequest(callback);
                break;
            case DETAILED:
                getFoodInfo(
                        getFoodIngredients(
                                getFoodInstructions(
                                        getFoodTags(null))))
                        .handleRequest(callback);
                break;
        }
    }

//    public static void getNextFoodsInfo(LinkedList<Food> actualList, FoodListCallback callback) {
//        new FoodListFirebaseHandler(actualList).handleRequest(callback);
//    }
//
//    public static void getNextFavorites(LinkedList<Food> actualList, FoodListCallback callback) {
//        String userId = UserModelController.getUserId();
//        if (userId == null || userId.isEmpty()) {
//            return;
//        }
//
//        new IndexedFoodListFirebaseHandler(mDatabaseReference.child(BOOKMARKS_PATH).child(userId), actualList).handleRequest(callback);
//    }

    private static FoodFirebaseHandler getFoodInfo(ChainedHandler<FoodCallback> nextHandler) {
        FoodInfoFirebaseHandler handler = new FoodInfoFirebaseHandler();
        handler.setNextHandler(nextHandler);
        return handler;
    }

    private static FoodFirebaseHandler getFoodIngredients(ChainedHandler<FoodCallback> nextHandler) {
        FoodIngredientsFirebaseHandler handler = new FoodIngredientsFirebaseHandler();
        handler.setNextHandler(nextHandler);
        return handler;
    }

    private static FoodFirebaseHandler getFoodInstructions(ChainedHandler<FoodCallback> nextHandler) {
        FoodInstructionsFirebaseHandler handler = new FoodInstructionsFirebaseHandler();
        handler.setNextHandler(nextHandler);
        return handler;
    }

    private static FoodFirebaseHandler getFoodTags(ChainedHandler<FoodCallback> nextHandler) {
        FoodTagsFirebaseHandler handler = new FoodTagsFirebaseHandler();
        handler.setNextHandler(nextHandler);
        return handler;
    }

    public static void saveFood(final Food food, OnSuccessListener<? super Void> infoSuccessListener, OnFailureListener infoFailureListener, OnProgressListener<? super UploadTask.TaskSnapshot> imageProgressListener, OnCompleteListener<UploadTask.TaskSnapshot> imageCompleteListener, OnFailureListener imageFailureListener) {
        if (food == null) {
            return;
        }

        if (food.getKey() == null) {
            String key = mDatabaseReference.child(FOOD_INFO_PATH).push().getKey();
            food.setKey(key);
        }

        food.setOwnerId(UserModelController.getUserId());

        String imagePath = FOOD_IMAGE_STORAGE_PATH + "/" + food.getKey();

        if (food.getImageTempUri() != null) {
            if (food.getImageRef() != null && !food.getImageRef().isEmpty()) {
                mStorageReference.child(food.getImageRef()).delete();
                imagePath = food.getImageRef() + "_";
            }

            food.setImageRef(imagePath);
            mStorageReference.child(imagePath).putFile(food.getImageTempUri())
                    .addOnProgressListener(imageProgressListener)
                    .addOnCompleteListener(imageCompleteListener)
                    .addOnFailureListener(imageFailureListener);
        } else if (food.getImageRef() == null) {
            mStorageReference.child(imagePath).delete();
        }

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(FOOD_INFO_PATH + "/" + food.getKey(), food);
        childUpdates.put(FOOD_INGREDIENTS_PATH + "/" + food.getKey(), food.getIngredients());
        childUpdates.put(FOOD_INSTRUCTIONS_PATH + "/" + food.getKey(), food.getInstructions());
        childUpdates.put(FOOD_TAGS_PATH + "/" + food.getKey(), food.getTags());

        mDatabaseReference.updateChildren(childUpdates)
                .addOnSuccessListener(infoSuccessListener)
                .addOnFailureListener(infoFailureListener);
    }

    public static void deleteFood(Food food) {
        if (food == null || food.getKey() == null || food.getKey().isEmpty()) {
            return;
        }

        List<String> paths = Arrays.asList(
                FOOD_INFO_PATH,
                FOOD_INGREDIENTS_PATH,
                FOOD_INSTRUCTIONS_PATH,
                FOOD_TAGS_PATH);

        for (String path : paths) {
            mDatabaseReference.child(path).child(food.getKey()).removeValue();
        }

        mStorageReference.child(FOOD_IMAGE_STORAGE_PATH).child(food.getKey()).delete();
    }

    public static DatabaseReference getFoodInfoReference() {
        return mDatabaseReference.child(FOOD_INFO_PATH);
    }
}
