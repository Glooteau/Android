package com.rstex.glooteau.food.view;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.rstex.glooteau.R;
import com.rstex.glooteau.common.view.AdditiveItem;

import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 06/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class IngredientItemView extends ConstraintLayout implements AdditiveItem {
    public interface IngredientItemViewModel {
        String Title();
        String Amount();
        String Unit();
        @DrawableRes int UnitImage();
    }

    private TextView mTitle;
    private TextView mAmount;
    private ImageView mUnitImageView;
    private AttributeSet mAttrs;

    public IngredientItemView(Context context) {
        super(context);
        init(context);
    }

    public IngredientItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        mAttrs = attrs;
    }

    private void init(Context context) {
        View rootView = inflate(context, R.layout.item_view_ingredient, this);

        mTitle = rootView.findViewById(R.id.titleTextView);
        mAmount = rootView.findViewById(R.id.amountTextView);
        mUnitImageView = rootView.findViewById(R.id.unitImageView);
    }

    public void setViewModel(@NonNull IngredientItemViewModel viewModel) {
        mTitle.setText(nullableToString(viewModel.Title()));
        String amount = nullableToString(viewModel.Amount()) + " " + nullableToString(viewModel.Unit());
        mAmount.setText(amount);
        mUnitImageView.setImageResource(viewModel.UnitImage());

        invalidate();
        requestLayout();
    }

    @Override
    public boolean isFilled() {
        return false;
    }

    @Override
    public void setOnFillListener(OnFillListener listener) {

    }

    @Override
    public AdditiveItem newInstance() {
        if (mAttrs != null){
            return new IngredientItemView(getContext(), mAttrs);
        }
        return new IngredientItemView(getContext());
    }
}
