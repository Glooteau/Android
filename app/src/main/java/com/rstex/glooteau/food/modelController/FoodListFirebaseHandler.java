package com.rstex.glooteau.food.modelController;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.rstex.glooteau.common.utils.CallbackValueEventListener;
import com.rstex.glooteau.food.model.Food;

import java.util.LinkedList;

import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_INFO_PATH;
import static com.rstex.glooteau.common.modelController.FirebasePaths.LIST_PAGE_SIZE;
import static java.util.Collections.reverse;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 20/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

class FoodListFirebaseHandler {
    private final LinkedList<Food> mPreviousList;

    FoodListFirebaseHandler(LinkedList<Food> previousList) {
        mPreviousList = previousList;
    }

    void handleRequest(FoodListCallback callback) {
        Query query = FirebaseDatabase.getInstance().getReference().child(FOOD_INFO_PATH).orderByKey();
        if (!mPreviousList.isEmpty()) {
            query = query.endAt(mPreviousList.peekLast().getKey());
        }
        query = query.limitToLast(LIST_PAGE_SIZE);

        query.addListenerForSingleValueEvent(new CallbackValueEventListener<FoodListCallback>(callback) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LinkedList<Food> foodList = new LinkedList<>(mPreviousList);
                LinkedList<Food> newItems = new LinkedList<>();

                for (DataSnapshot foodSnapshot : dataSnapshot.getChildren()) {
                    if (!mPreviousList.isEmpty() && foodSnapshot.getKey().equals(mPreviousList.getLast().getKey())) {
                        continue;
                    }

                    Food food = foodSnapshot.getValue(Food.class);
                    if (food != null) {
                        food.setKey(foodSnapshot.getKey());
                        newItems.add(food);
                    }
                }

                reverse(newItems);
                foodList.addAll(newItems);

                getCallback().onSuccess(new FoodListCallback.FoodListResponse(foodList, mPreviousList.size(), newItems.size()));
            }
        });
    }
}
