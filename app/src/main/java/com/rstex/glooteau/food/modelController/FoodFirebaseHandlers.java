package com.rstex.glooteau.food.modelController;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rstex.glooteau.common.utils.CallbackValueEventListener;
import com.rstex.glooteau.common.utils.ChainedHandler;
import com.rstex.glooteau.food.model.Food;
import com.rstex.glooteau.food.model.Ingredient;

import java.util.ArrayList;
import java.util.List;

import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_INFO_PATH;
import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_INGREDIENTS_PATH;
import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_INSTRUCTIONS_PATH;
import static com.rstex.glooteau.food.modelController.FirebasePaths.FOOD_TAGS_PATH;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 19/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 *
 * <h1>FoodFirebaseHandler</h1>
 * Fetches and builds Food objects progressively from Firebase Database.
 * Uses of FoodInfoFirebaseHandler replaces all Food's content, so should be the first in chains to don't discard previous results.
 */
abstract class FoodFirebaseHandler extends ChainedHandler<FoodCallback> {
    static final DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference();

    @Override
    public void handleRequest(FoodCallback callback) {
        if (callback.getFood() == null) {
            callback.setFood(new Food());
            callback.getFood().setKey(callback.getKey());
        }
    }

    @Override
    public void finaliseRequest(FoodCallback callback) {
        callback.onSuccess(callback.getFood());
    }
}

/**
 * Should be used only as the first node of the chain.
 */
class FoodInfoFirebaseHandler extends FoodFirebaseHandler {
    @Override
    public void handleRequest(FoodCallback callback) {
        mDatabaseReference.child(FOOD_INFO_PATH + "/" + callback.getKey()).addListenerForSingleValueEvent(new CallbackValueEventListener<FoodCallback>(callback) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                getCallback().setFood(dataSnapshot.getValue(Food.class));

                if (getCallback().getFood() == null) {
                    String message = "Food property hasn't been set on " + getCallback();
                    FirebaseCrash.report(new Exception(message));
                    getCallback().onError(new Error(message));
                    return;
                }

                getCallback().getFood().setKey(dataSnapshot.getKey());
                handOverToNextHandler(getCallback());
            }
        });
    }
}

class FoodIngredientsFirebaseHandler extends FoodFirebaseHandler {
    @Override
    public void handleRequest(FoodCallback callback) {
        super.handleRequest(callback);

        mDatabaseReference.child(FOOD_INGREDIENTS_PATH + "/" + callback.getKey()).addListenerForSingleValueEvent(new CallbackValueEventListener<FoodCallback>(callback) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Ingredient> ingredients = new ArrayList<>();
                for (DataSnapshot ingredientSnapshot : dataSnapshot.getChildren()) {
                    ingredients.add(ingredientSnapshot.getValue(Ingredient.class));
                }
                getCallback().getFood().setIngredients(ingredients);
                handOverToNextHandler(getCallback());
            }
        });
    }
}

class FoodInstructionsFirebaseHandler extends FoodFirebaseHandler {
    @Override
    public void handleRequest(FoodCallback callback) {
        super.handleRequest(callback);

        mDatabaseReference.child(FOOD_INSTRUCTIONS_PATH + "/" + callback.getKey()).addListenerForSingleValueEvent(new CallbackValueEventListener<FoodCallback>(callback) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> instructions = new ArrayList<>();
                for (DataSnapshot instructionSnapshot : dataSnapshot.getChildren()) {
                    instructions.add(instructionSnapshot.getValue(String.class));
                }
                getCallback().getFood().setInstructions(instructions);
                handOverToNextHandler(getCallback());
            }
        });
    }
}

class FoodTagsFirebaseHandler extends FoodFirebaseHandler {
    @Override
    public void handleRequest(FoodCallback callback) {
        super.handleRequest(callback);

        mDatabaseReference.child(FOOD_TAGS_PATH + "/" + callback.getKey()).addListenerForSingleValueEvent(new CallbackValueEventListener<FoodCallback>(callback) {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> tagList = new ArrayList<>();
                for (DataSnapshot tagSnapshot : dataSnapshot.getChildren()) {
                    tagList.add(tagSnapshot.getKey());
                }
                getCallback().getFood().setTags(tagList);
                handOverToNextHandler(getCallback());
            }
        });
    }
}
