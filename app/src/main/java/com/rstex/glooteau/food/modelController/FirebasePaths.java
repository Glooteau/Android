package com.rstex.glooteau.food.modelController;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 20/09/17.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

interface FirebasePaths {
    //RealTimeDatabase - Data
    String FOOD_PATH = "food";
    String FOOD_INFO_PATH = FOOD_PATH + "/info";
    String FOOD_TAGS_PATH = FOOD_PATH + "/tags";
    String FOOD_INGREDIENTS_PATH = FOOD_PATH + "/ingredients";
    String FOOD_INSTRUCTIONS_PATH = FOOD_PATH + "/instructions";

    //RealTimeDatabase - Index
    String FOOD_PUBLIC_INDEX_PATH = FOOD_PATH + "/publicIndex";

    //Storage
    String FOOD_IMAGE_STORAGE_PATH = FOOD_PATH + "/image";
}
