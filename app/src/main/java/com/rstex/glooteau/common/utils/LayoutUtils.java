package com.rstex.glooteau.common.utils;

import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewCompat;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 13/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class LayoutUtils {

    public static void lockAppBarLayoutScrolling(final AppBarLayout appBarLayout, final boolean lock) {
        appBarLayout.setExpanded(!lock);
        ViewCompat.setNestedScrollingEnabled(appBarLayout, !lock);
    }
}
