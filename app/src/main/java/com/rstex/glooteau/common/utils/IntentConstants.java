package com.rstex.glooteau.common.utils;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 17/10/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class IntentConstants {
    public static final String DETAIL_FRAGMENT_CLASS = "context";
    public static final String KEY = "key";
    public static final String SCOPE = "scope";

    public enum Scope {
        Business,
        Feed,
        Menu
    }
}
