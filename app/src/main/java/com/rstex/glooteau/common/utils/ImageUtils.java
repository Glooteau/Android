package com.rstex.glooteau.common.utils;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.Fragment;

import com.google.firebase.crash.FirebaseCrash;
import com.rstex.glooteau.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.support.v4.content.ContextCompat.checkSelfPermission;
import static android.support.v4.content.FileProvider.getUriForFile;
import static java.io.File.createTempFile;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 11/10/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class ImageUtils {

    public static Uri pickImage(Fragment fragment, int cameraPermissionCode, int requestCode, boolean requestCamera) {
        if (requestCamera && checkSelfPermission(fragment.getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            fragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, cameraPermissionCode);
            return null;
        }

        Uri cameraImageUri = createImageFile(fragment);

        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setTypeAndNormalize("image/*");

        Intent chooser = Intent.createChooser(galleryIntent, null);

        if (requestCamera) {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, cameraImageUri);
            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{cameraIntent});
        }

        if (chooser.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
            fragment.startActivityForResult(chooser, requestCode);
        }

        return cameraImageUri;
    }

    private static Uri createImageFile(Fragment fragment) {
        File imageFile = null;

        try {
            String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
            imageFile = createTempFile(timestamp, ".jpg",
                    fragment.getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES));
        } catch (IOException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }

        if (imageFile != null) {
            return getUriForFile(fragment.getContext(), fragment.getResources().getString(R.string.file_provider), imageFile);
        }
        return null;
    }
}
