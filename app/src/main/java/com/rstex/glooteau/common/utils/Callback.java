package com.rstex.glooteau.common.utils;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 20/09/17.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public interface Callback<T> {
    void onSuccess(T response);
    void onError(Error error);
}
