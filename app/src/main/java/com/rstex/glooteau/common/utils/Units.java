package com.rstex.glooteau.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 09/10/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class Units {

    public static List<String> QuantityUnits() {
        return Collections.singletonList("un");
    }

    public static List<String> MassUnits() {
        return Arrays.asList("kg", "g", "mg");
    }

    public static List<String> LiquidVolumeUnits() {
        return Arrays.asList("L", "mL");
    }

    public static List<Currency> AllCurrencies() {
        ArrayList<Currency> currencies = new ArrayList<>();
        //TODO: Use sets
        for (Locale locale : Locale.getAvailableLocales()) {
            for (String country : Locale.getISOCountries()) {
                if (country.equalsIgnoreCase(locale.getCountry())) {
                    Currency currency = Currency.getInstance(locale);
                    if (currency != null){
                        currencies.add(currency);
                        break;
                    }
                }
            }
        }
        return currencies;
    }

    public static List<String> CurrencySymbols() {
        ArrayList<String> displays = new ArrayList<>();
        //TODO: Use sets
        for (Currency currency : AllCurrencies()) {
            if (!displays.contains(currency.getSymbol())) {
                displays.add(currency.getSymbol());
            }
        }
        Collections.sort(displays);
        return displays;
    }
}
