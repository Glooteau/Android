package com.rstex.glooteau.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.widget.CheckedTextView;

import com.rstex.glooteau.R;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 18/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class WeekdaysView extends ConstraintLayout {
    private SparseIntArray mSubviews = new SparseIntArray();

    public WeekdaysView(Context context) {
        super(context);
        init(context, null, 0);
    }

    public WeekdaysView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public WeekdaysView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        inflate(context, R.layout.weekdays_view, this);

        boolean selectable = true;

        if (attrs != null) {
            TypedArray attrsArray = context.obtainStyledAttributes(attrs, R.styleable.WeekdaysView, defStyleAttr, 0);
            selectable = attrsArray.getBoolean(R.styleable.WeekdaysView_selectable, true);
            attrsArray.recycle();
        }

        mSubviews.put(Calendar.SUNDAY, R.id.sunCheckedTextView);
        mSubviews.put(Calendar.MONDAY, R.id.monCheckedTextView);
        mSubviews.put(Calendar.TUESDAY, R.id.tueCheckedTextView);
        mSubviews.put(Calendar.WEDNESDAY, R.id.wedCheckedTextView);
        mSubviews.put(Calendar.THURSDAY, R.id.thuCheckedTextView);
        mSubviews.put(Calendar.FRIDAY, R.id.friCheckedTextView);
        mSubviews.put(Calendar.SATURDAY, R.id.satCheckedTextView);

        String[] weekdays = DateFormatSymbols.getInstance(Locale.getDefault()).getWeekdays();

        for (int index = 0; index < mSubviews.size(); index++) {
            CheckedTextView item = findViewById(mSubviews.valueAt(index));

            item.setText(nullableToString(weekdays[mSubviews.keyAt(index)].charAt(0)).toUpperCase());
            if (selectable) {
                item.setOnClickListener(v -> ((CheckedTextView) v).toggle());
            }
        }
    }

    public List<String> getSelectedWeekdays() {
        String[] weekdays = DateFormatSymbols.getInstance(Locale.US).getWeekdays();
        ArrayList<String> selectedWeekdays = new ArrayList<>();
        for (int index = 0; index < mSubviews.size(); index++) {
            CheckedTextView item = findViewById(mSubviews.valueAt(index));

            if (item.isChecked()) {
                selectedWeekdays.add(weekdays[mSubviews.keyAt(index)]);
            }
        }
        return selectedWeekdays;
    }

    public void setSelectedWeekdays(List<String> selectedWeekdays) {
        for (int index = 0; index < mSubviews.size(); index++) {
            CheckedTextView item = findViewById(mSubviews.valueAt(index));
            item.setChecked(false);
        }

        String[] weekdays = DateFormatSymbols.getInstance(Locale.US).getWeekdays();
        for (int index = 0; index < weekdays.length; index++) {
            if (selectedWeekdays.contains(weekdays[index])) {
                CheckedTextView item = findViewById(mSubviews.valueAt(index));
                item.setChecked(true);
            }
        }
    }
}
