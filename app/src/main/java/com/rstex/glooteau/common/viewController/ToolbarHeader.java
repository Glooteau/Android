package com.rstex.glooteau.common.viewController;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.rstex.glooteau.R;
import com.rstex.glooteau.user.modelController.UserModelController;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 19/10/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class ToolbarHeader extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_toolbar_header, container, false);
        final ImageView userImageView = view.findViewById(R.id.userImageView);

        Glide.with(getContext())
                .load(UserModelController.getUserImage())
                .apply(RequestOptions.circleCropTransform())
                .into(userImageView);

        return view;
    }
}
