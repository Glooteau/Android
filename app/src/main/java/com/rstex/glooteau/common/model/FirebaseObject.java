package com.rstex.glooteau.common.model;

import com.google.firebase.database.Exclude;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 12/09/17.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class FirebaseObject {
    private String mKey;

    protected FirebaseObject() {
        mKey = null;
    }

    protected FirebaseObject(String key) {
        mKey = key;
    }

    @Exclude
    public final String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        mKey = key;
    }
}
