package com.rstex.glooteau.common.view;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 22/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public interface AdditiveItem {

    interface OnFillListener {
        void onFill(AdditiveItem item);
    }

    boolean isFilled();
    void setOnFillListener(OnFillListener listener);
    AdditiveItem newInstance();
}
