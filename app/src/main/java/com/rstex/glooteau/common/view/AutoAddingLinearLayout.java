package com.rstex.glooteau.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rstex.glooteau.R;

import java.util.ArrayList;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 07/10/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class AutoAddingLinearLayout extends LinearLayout {
    @DrawableRes private int mRemoveButtonDrawable;
    private boolean mRemovableItems;
    private Drawable mIndexBackground;
    @ColorInt private int mIndexTextColor;
    private boolean mShowIndex;
    private ArrayList<AdditiveItem> mAdditiveItems = new ArrayList<>();

    public AutoAddingLinearLayout(Context context) {
        super(context);
    }

    public AutoAddingLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public AutoAddingLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        TypedValue foregroundColor = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.colorForeground, foregroundColor, true);

        TypedArray attrsArray = context.obtainStyledAttributes(attrs, R.styleable.AutoAddingLinearLayout, 0, 0);
        mRemoveButtonDrawable = attrsArray.getResourceId(R.styleable.AutoAddingLinearLayout_removeButtonDrawable, R.drawable.ic_clear_black_24dp);
        mRemovableItems = attrsArray.getBoolean(R.styleable.AutoAddingLinearLayout_removableItems, true);
        mIndexBackground = attrsArray.getDrawable(R.styleable.AutoAddingLinearLayout_indexBackground);
        mIndexTextColor = attrsArray.getColor(R.styleable.AutoAddingLinearLayout_indexTextColor, foregroundColor.resourceId);
        mShowIndex = attrsArray.getBoolean(R.styleable.AutoAddingLinearLayout_showIndex, false);

        attrsArray.recycle();
    }

    @Override
    public void addView(final View child, int index, ViewGroup.LayoutParams params) {
        if (!(child instanceof AdditiveItem)) {
            throw new AssertionError("View being added must conform to AdditiveItem interface");
        }

        ((AdditiveItem) child).setOnFillListener(item -> {
            for (AdditiveItem additiveItem : mAdditiveItems) {
                if(!additiveItem.isFilled()) {
                    return;
                }
            }
            addView((View)item.newInstance());
        });

        final View item = inflate(getContext(), R.layout.auto_adding_item, null);

        TextView indexTextView = item.findViewById(R.id.indexTextView);
        indexTextView.setVisibility(mShowIndex ? VISIBLE : INVISIBLE);
        indexTextView.setTextColor(mIndexTextColor);
        indexTextView.setBackground(mIndexBackground);

        FrameLayout container = item.findViewById(R.id.itemContainer);
        container.addView(child, params);

        ImageButton removeButton = item.findViewById(R.id.removeButton);
        removeButton.setImageResource(mRemoveButtonDrawable);
        removeButton.setOnClickListener(view -> removeView(item));

        super.addView(item, index, params);
        mAdditiveItems.add(indexOfChild(item), (AdditiveItem) child);
    }

    @Override
    public void removeView(View view) {
        mAdditiveItems.remove(indexOfChild(view));
        super.removeView(view);
    }

    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        TextView indexTextView = child.findViewById(R.id.indexTextView);
        indexTextView.setText(String.valueOf(indexOfChild(child) + 1));

        ImageButton removeButton = child.findViewById(R.id.removeButton);
        removeButton.setVisibility(mRemovableItems && indexOfChild(child) != getChildCount() - 1 ? VISIBLE : GONE);

        return super.drawChild(canvas, child, drawingTime);
    }

    public ArrayList<AdditiveItem> getAdditiveItems() {
        return new ArrayList<>(mAdditiveItems);
    }
}
