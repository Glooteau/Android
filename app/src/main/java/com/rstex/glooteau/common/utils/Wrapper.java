package com.rstex.glooteau.common.utils;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 20/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public final class Wrapper<T> {
    public T value;

    public Wrapper(T value) {
        this.value = value;
    }
}
