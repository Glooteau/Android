package com.rstex.glooteau.common.viewController;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.crash.FirebaseCrash;
import com.rstex.glooteau.R;

import java.util.Calendar;

import static java.util.Calendar.YEAR;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 15/10/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class AboutDialogFragment extends DialogFragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.AboutTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);

        ImageButton closeButton = view.findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        PackageInfo packageInfo = null;

        try {
            packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }

        if (packageInfo == null) {
            return view;
        }

        TextView versionTextView = view.findViewById(R.id.versionTextView);
        versionTextView.setText(getString(R.string.app_name) + " " + packageInfo.versionName);

        TextView copyrightTextView = view.findViewById(R.id.copyrightTextView);
        copyrightTextView.setText("© " + Calendar.getInstance().get(YEAR) + " RSTex");

        return view;
    }
}
