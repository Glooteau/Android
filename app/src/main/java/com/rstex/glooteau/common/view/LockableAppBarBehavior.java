package com.rstex.glooteau.common.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 14/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class LockableAppBarBehavior extends AppBarLayout.Behavior {
    LockableAppBarBehavior() {
        super();
        this.setDragCallback(new DragCallback() {
            @Override
            public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                return ViewCompat.isNestedScrollingEnabled(appBarLayout);
            }
        });
    }

    public LockableAppBarBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setDragCallback(new DragCallback() {
            @Override
            public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                return ViewCompat.isNestedScrollingEnabled(appBarLayout);
            }
        });
    }
}
