package com.rstex.glooteau.common.utils;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 20/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class CallbackValueEventListener<T extends Callback> implements ValueEventListener {
    private final T mCallback;

    public CallbackValueEventListener(T callback) {
        mCallback = callback;
    }

    public T getCallback() {
        return mCallback;
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        FirebaseCrash.report(new Exception(databaseError.toException()));
        mCallback.onError(new Error(databaseError.toException()));
    }
}
