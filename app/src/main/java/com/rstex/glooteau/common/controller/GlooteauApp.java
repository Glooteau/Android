package com.rstex.glooteau.common.controller;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 13/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class GlooteauApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
