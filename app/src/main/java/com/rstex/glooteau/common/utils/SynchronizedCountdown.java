package com.rstex.glooteau.common.utils;

import android.os.Handler;
import android.os.Looper;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 12/10/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class SynchronizedCountdown {
    private final Wrapper<Long> syncLockCountdown;

    public SynchronizedCountdown(final Runnable runnable, Long taskNumber) {
        syncLockCountdown = new Wrapper<>(taskNumber);
        final Handler handler = new Handler(Looper.getMainLooper());
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    synchronized (syncLockCountdown) {
                        while (syncLockCountdown.value != 0) {
                            syncLockCountdown.wait();
                        }
                        handler.post(runnable);
                    }
                } catch (InterruptedException e) {
                    FirebaseCrash.report(e);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public synchronized void decrement() {
        synchronized (syncLockCountdown) {
            syncLockCountdown.value--;
            syncLockCountdown.notify();
        }
    }
}
