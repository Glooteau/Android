package com.rstex.glooteau.common.viewController;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import com.rstex.glooteau.user.modelController.UserModelController;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 09/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO);

        UserModelController.ensureSignIn();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
