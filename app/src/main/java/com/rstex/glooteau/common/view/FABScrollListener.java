package com.rstex.glooteau.common.view;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 13/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class FABScrollListener extends RecyclerView.OnScrollListener {

    private FloatingActionButton mFAB;

    public FABScrollListener(@NonNull FloatingActionButton fab) {
        mFAB = fab;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0 && mFAB.isShown())
            mFAB.hide();
        else if (dy < 0 && !mFAB.isShown())
            mFAB.show();
    }
}
