package com.rstex.glooteau.common.utils;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 19/09/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class ChainedHandler<T> {
    private ChainedHandler<T> mNextHandler;

    public ChainedHandler<T> getNextHandler() {
        return mNextHandler;
    }

    public void setNextHandler(ChainedHandler<T> nextHandler) {
        this.mNextHandler = nextHandler;
    }

    protected final void handOverToNextHandler(T request) {
        if (mNextHandler != null) {
            mNextHandler.handleRequest(request);
        } else {
            this.finaliseRequest(request);
        }
    }

    public abstract void handleRequest(T request);
    protected abstract void finaliseRequest(T request);
}
