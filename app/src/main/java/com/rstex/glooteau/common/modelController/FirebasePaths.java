package com.rstex.glooteau.common.modelController;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 15/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public interface FirebasePaths {
    //Tag Index
    String TAGS_PATH = "tagIndex";
    String TAG_POOL_PATH = TAGS_PATH + "/pool";
    String TAGGED_FOOD_INDEX_PATH = TAGS_PATH + "/taggedFood";
    String TAGGED_BUSINESS_INDEX_PATH = TAGS_PATH + "/taggedBusiness";

    //Constant
    Integer LIST_PAGE_SIZE = 10;
}
