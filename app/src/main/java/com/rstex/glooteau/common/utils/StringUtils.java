package com.rstex.glooteau.common.utils;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 04/10/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public abstract class StringUtils {

    public static String nullableToString(Object object) {
        if (object == null) {
            return "";
        }
        return String.valueOf(object);
    }
}
