package com.rstex.glooteau.common.view;

import android.support.constraint.Group;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rstex.glooteau.R;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 20/11/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class MenuCardViewHolder extends RecyclerView.ViewHolder { //FIXME: Use ViewModel
    public TextView mTitle;
    public TextView mDescription;
    public TextView mPrice;
    public TextView mTime;
    public TextView mTags;
    public WeekdaysView mWeekdaysView;
    public Group mTagsGroup;
    public Group mTimesGroup;

    public MenuCardViewHolder(View itemView) {
        super(itemView);
        mTitle = itemView.findViewById(R.id.title);
        mDescription = itemView.findViewById(R.id.description);
        mPrice = itemView.findViewById(R.id.priceRangeTextView);
        mTime = itemView.findViewById(R.id.timeRangeTextView);
        mWeekdaysView = itemView.findViewById(R.id.weekdaysView);
        mTags = itemView.findViewById(R.id.tagsTextView);
        mTagsGroup = itemView.findViewById(R.id.tagsGroup);
        mTimesGroup = itemView.findViewById(R.id.timesGroup);
    }
}
