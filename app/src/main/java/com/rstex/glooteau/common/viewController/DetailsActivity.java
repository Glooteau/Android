package com.rstex.glooteau.common.viewController;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.rstex.glooteau.R;

import java.io.Serializable;

import static com.rstex.glooteau.common.utils.IntentConstants.DETAIL_FRAGMENT_CLASS;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 22/09/17.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Serializable detailSerializableExtra = getIntent().getSerializableExtra(DETAIL_FRAGMENT_CLASS);

        if (detailSerializableExtra == null) {
            throw new AssertionError("'DETAIL_FRAGMENT_CLASS' not set into intent extras to " + this);
        }

        setContentView(R.layout.activity_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> NavUtils.navigateUpFromSameTask(DetailsActivity.this));

        Fragment detailFragment;

        if (Class.class.isInstance(detailSerializableExtra)) {
            Class detailClass = (Class) detailSerializableExtra;

            if (Fragment.class.isAssignableFrom(detailClass)) {
                try {
                    detailFragment = (Fragment) detailClass.newInstance();
                } catch (Exception e) {
                    throw new IllegalStateException("Failed to instantiate Fragment Class (" + detailClass + ") set into 'DETAIL_FRAGMENT_CLASS' intent extras to " + this, e);
                }
            } else {
                throw new AssertionError("Class (" + detailClass + ") set into 'DETAIL_FRAGMENT_CLASS' intent extras to " + this + " isn't a Class<? extends Fragment> compatible type");
            }
        } else {
            throw new AssertionError("Value (" + detailSerializableExtra + ") set into 'DETAIL_FRAGMENT_CLASS' intent extras to " + this + " isn't a Class<? extends Fragment> reference");
        }

        if (detailFragment == null) {
            return;
        }

        detailFragment.setArguments(getIntent().getExtras());

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, detailFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}
