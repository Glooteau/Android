package com.rstex.glooteau.common.viewController;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.rstex.glooteau.R;
import com.rstex.glooteau.business.viewController.BusinessMainMapFragment;
import com.rstex.glooteau.food.viewController.FoodMainFragment;
import com.rstex.glooteau.user.modelController.UserModelController;
import com.rstex.glooteau.user.viewController.UserMainFragment;

import static com.rstex.glooteau.common.utils.StringUtils.nullableToString;

/**
 * Glooteau
 *
 * Created by Ricardo Carvalho on 23/08/2017.
 * Copyright © 2017 Ricardo Carvalho. All rights reserved.
 */

public class MainActivity extends AppCompatActivity {
    private static final int LOCATION_REQUEST_CODE = 6584;
    private static final String LAST_INDEX = "lastIndex";
    private int mLastIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNav = findViewById(R.id.bottom_nav);
        bottomNav.setOnNavigationItemSelectedListener(item -> switchFragment(item.getItemId()));

        if (!nullableToString(UserModelController.getUserName()).isEmpty()) {
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle(UserModelController.getUserName());
        }

        if (savedInstanceState == null) {
            int itemId = UserModelController.isSignedIn() ? R.id.navigation_food : R.id.navigation_user;
            bottomNav.getMenu().findItem(itemId).setChecked(true);
            switchFragment(itemId);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(LAST_INDEX, mLastIndex);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mLastIndex = savedInstanceState.getInt(LAST_INDEX);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    private boolean switchFragment(int id) {
        FloatingActionButton fab = findViewById(R.id.fab);
        if (mLastIndex != id) {
            fab.hide();
        }

        mLastIndex = id;

        Fragment fragment;

        switch (id) {
            case R.id.navigation_user:
                fragment = new UserMainFragment();
                break;
            case R.id.navigation_food:
                fragment = new FoodMainFragment();
                break;
            case R.id.navigation_nearby:
                fragment = new BusinessMainMapFragment();
                break;
            default:
                return false;
        }

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();

        return true;
    }
}
