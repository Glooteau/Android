# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# Firebase Realtime Database rules
-keepattributes Signature
-keepattributes *Annotation*

# Models
-keepclassmembers class com.rstex.glooteau.common.model.** {
  *;
}

-keepclassmembers class com.rstex.glooteau.food.model.** {
  *;
}

-keepclassmembers class com.rstex.glooteau.money.model.** {
  *;
}

-keepclassmembers class com.rstex.glooteau.user.model.** {
  *;
}

# ViewHolders
-keep class com.rstex.glooteau.food.view.FeedCardViewHolder {
  *;
}

-keep class com.rstex.glooteau.menu.view.MenuFoodCardViewHolder {
  *;
}
